Unzip this file to a directory. If you want to preserve data, it is stored in the data/ directory.

Edit tortugaServiceWrapper.xml to fit your needs (actually, it works out of the box) using instructions from https://github.com/kohsuke/winsw

Install: tortugaServiceWrapper install
Start: tortugaServiceWrapper start (or use Windows capabilities)
Stop: tortugaServiceWrapper stop (or use Windows capabilities)
Uninstall: tortugaServiceWrapper uninstall

