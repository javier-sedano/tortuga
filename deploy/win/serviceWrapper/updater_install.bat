cd %~dp0
set BASEPATH=%~dp0
powershell -Command "(gc TortugaUpdater_template.xml) -Replace 'BASEPATH', '%BASEPATH%' | Out-File -Encoding ASCII TortugaUpdater.xml"

schtasks /delete /tn "TortugaUpdater" /f
schtasks /create /xml .\TortugaUpdater.xml /tn "TortugaUpdater" /ru "SYSTEM"
