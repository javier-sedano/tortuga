#!/bin/bash
cd "$(dirname "$0")"
TAG=latest
if [ "$1" ]
then
  TAG="$1"
fi
REGISTRY=registry.gitlab.com/javier-sedano/tortuga/app
IMAGE=$REGISTRY:$TAG
docker container stop tortuga
docker container rm tortuga
docker image ls | grep $REGISTRY | while read LINE
do
  OLD_TAG=`echo $LINE | cut -d\  -f2`
  docker image rm $REGISTRY:$OLD_TAG
done
docker pull $IMAGE
docker container create -m 160m -p 5280:8080 -v /data/tortuga/:/app/data/ -e spring.flyway.locations=classpath:/db/migration,classpath:/db/sample --restart unless-stopped --name tortuga $IMAGE
docker container start tortuga
