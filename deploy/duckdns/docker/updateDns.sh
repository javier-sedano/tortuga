#!/bin/bash
gcloud auth activate-service-account --key-file=/etc/duckdns-cronjob-gke/key.json
gcloud config set project jdocker
gcloud config set compute/zone us-east1-b
gcloud container clusters get-credentials tortuga

IP=""
MAX=3
if [ "$1" ]
then
  MAX=$1
fi

while [ -z "$IP" ]
do
  echo "Countdown tries: $MAX "
  sleep 10
  kubectl get ingress tortuga-https-ingress -o json
  IP=$(kubectl get ingress tortuga-https-ingress -o=jsonpath='{.status.loadBalancer.ingress[0].ip}')
  [ $MAX -gt 0 ] || break
  MAX=$((MAX-1))
done
if [ "$IP" ]
then
  echo "Updating IP to $IP"
  echo url="https://www.duckdns.org/update?domains=tortuga-jsedano&token=$DUCKDNS_TOKEN&ip=$IP" | curl -K -
else
  echo "Unable to find IP, giving up"
fi
