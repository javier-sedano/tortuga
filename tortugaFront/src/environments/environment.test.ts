import { NgxLoggerLevel } from 'ngx-logger';

export const environment = {
  production: false,
  ngxLogLevel: NgxLoggerLevel.DEBUG,
  debounceTime: 0,
  delayedFeedbackTime: 0,
};
