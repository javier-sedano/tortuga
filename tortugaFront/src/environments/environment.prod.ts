import { NgxLoggerLevel } from 'ngx-logger';

export const environment = {
  production: true,
  ngxLogLevel: NgxLoggerLevel.INFO,
  debounceTime: 300,
  delayedFeedbackTime: 300,
};
