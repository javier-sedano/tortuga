import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { NGX_TRANSLATE_CONF } from './../app.module';
import { NotesRoutingModule } from './notes-routing.module';
import { NotesComponent } from './notes.component';

@NgModule({
  declarations: [
    NotesComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule.forChild(NGX_TRANSLATE_CONF),
    NotesRoutingModule,
    InfiniteScrollModule,
    FontAwesomeModule,
  ]
})
export class NotesModule { }
