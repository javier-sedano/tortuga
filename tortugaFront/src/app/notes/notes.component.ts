import { NGXLogger } from 'ngx-logger';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Subject, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';
import { faSearch, faPlus, faSave, faTrash, faSync, faAngleLeft } from '@fortawesome/free-solid-svg-icons';

import { DebouncerProviderService } from 'src/app/struct/debouncer-provider/debouncer-provider.service';
import { delayedFeedbackDo } from 'src/app/struct/utils';
import { NotesService } from './notes-service/notes.service';
import { DetailedNote } from './notes-service/detailed-note';
import { MinimalNote } from './notes-service/minimal-note';
import { ErrorService } from '../error/error.service';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
})
export class NotesComponent implements OnInit {

  filter: string = "";
  notes: MinimalNote[] = [];
  selectedNote: DetailedNote;
  showSmallSearchPanel: boolean = false;
  isFinding: boolean = false;
  isSaving: boolean = false;
  isDeleting: boolean = false;
  isAdding: boolean = false;
  isGetting: boolean = false;
  faSearch = faSearch;
  faPlus = faPlus;
  faSave = faSave;
  faTrash = faTrash;
  faSync = faSync;
  faAngleLeft = faAngleLeft;

  private debouncerSubject: Subject<string> = new Subject<string>();

  constructor(
    private logger: NGXLogger,
    private activatedRoute: ActivatedRoute,
    private notesService: NotesService,
    private errorService: ErrorService,
    private debouncerProvider: DebouncerProviderService,
    private router: Router,
  ) { }

  private loadInformedItem() {
    let selectingDelayedFeedbackDoer: any;
    this.activatedRoute.paramMap.pipe(
      switchMap(
        (params: ParamMap) => {
          this.isGetting = true;
          selectingDelayedFeedbackDoer = delayedFeedbackDo(() => { this.selectedNote = undefined; });
          let id: string = params.get('id');
          if (id == "") {
            return of(undefined);
          }
          return this.notesService.getNote(Number(params.get('id')))
            .pipe(catchError(error => {
              this.errorService.handleError(error);
              return of(undefined);
            }));
        }
      )
    ).subscribe( // NOSONAR
      note => {
        this.isGetting = false;
        selectingDelayedFeedbackDoer.undo(() => {
          // Intentionally blank, nothing to undo
        });
        this.selectedNote = note
        this.showSmallSearchPanel = false;
      }
    );
  }

  private buildDebouncer() {
    this.debouncerProvider.buildDebouncer(
      this.debouncerSubject,
      "",
      (debouncedFilter: string) => {
        this.logger.trace("debounced filter:", debouncedFilter);
        this.isFinding = true;
        return this.notesService.findNotes(debouncedFilter, 0);
      }
    ).subscribe( // NOSONAR
      notes => {
        this.isFinding = false;
        document.querySelector("#notes-search-results").scrollTop = 0;
        this.notes = notes;
      },
      error => {
        this.handleFindError(error);
      }
    );
  }

  private handleFindError(error) {
    this.isFinding = false;
    this.notes = [];
    this.errorService.handleError(error);
  }

  ngOnInit() {
    this.loadInformedItem();
    this.buildDebouncer();
  }

  debounceFilter() {
    this.logger.trace("debouncing filter:", this.filter);
    this.isFinding = true;
    this.debouncerSubject.next(this.filter);
  }

  add() {
    this.isAdding = true;
    this.notesService.createNote().subscribe( // NOSONAR
      noteId => {
        this.isAdding = false;
        this.debounceFilter();
        this.router.navigate(["../" + noteId], { relativeTo: this.activatedRoute });
      },
      error => {
        this.isAdding = false;
        this.errorService.handleError(error);
      }
    );
  }

  toggleShowSmallSearchPanel() {
    this.showSmallSearchPanel = !this.showSmallSearchPanel;
  }

  findMore() {
    if (this.isFinding) {
      this.logger.trace("Was already finding, skipping");
      return;
    }
    this.isFinding = true;
    this.notesService.findNotes(this.filter, this.notes.length).subscribe( // NOSONAR
      notes => {
        this.notes = this.notes.concat(notes);
        this.isFinding = false;
      },
      error => {
        this.handleFindError(error);
      }
    );
  }

  save() {
    this.isSaving = true;
    this.notesService.updateNote(this.selectedNote).subscribe( // NOSONAR
      savedNote => {
        this.selectedNote = savedNote;
        this.isSaving = false;
        this.debounceFilter();
      },
      error => {
        this.isSaving = false;
        this.errorService.handleError(error);
      }
    );
  }

  delete() {
    this.isDeleting = true;
    this.notesService.deleteNote(this.selectedNote.id).subscribe( // NOSONAR
      () => {
        this.router.navigate(['../'], { relativeTo: this.activatedRoute });
        this.selectedNote = undefined;
        this.isDeleting = false;
        this.debounceFilter();
      },
      error => {
        this.isDeleting = false;
        this.errorService.handleError(error);
      }
    );
  }

}
