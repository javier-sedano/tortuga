import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { NGXLogger } from 'ngx-logger';

import { BASE_REST_URL } from 'src/app/app.constants';import { MinimalNote } from './minimal-note';
import { DetailedNote } from './detailed-note';

@Injectable({
  providedIn: 'root'
})
export class NotesService {

  private static readonly NOTES_URL = BASE_REST_URL + "notes";
  private static readonly NOTES_FILTER_URL = NotesService.NOTES_URL;
  private static readonly NOTES_NOTE_URL = NotesService.NOTES_URL + "/";

  constructor(private logger: NGXLogger, private http: HttpClient) { }

  findNotes(filter: string, from: number): Observable<MinimalNote[]> {
    return this.http.get<MinimalNote[]>(NotesService.NOTES_FILTER_URL, { params: { filter: filter, from: from.toString() } });
  }

  getNote(id: number): Observable<DetailedNote> {
    return this.http.get<DetailedNote>(NotesService.NOTES_NOTE_URL + id);
  }

  updateNote(note: DetailedNote): Observable<DetailedNote> {
    return this.http.put<DetailedNote>(NotesService.NOTES_URL, note);
  }

  deleteNote(id: number): Observable<any> {
    return this.http.delete<string>(NotesService.NOTES_NOTE_URL + id);
  }

  createNote(): Observable<number> {
    return this.http.post<number>(NotesService.NOTES_URL, null);
  }

}
