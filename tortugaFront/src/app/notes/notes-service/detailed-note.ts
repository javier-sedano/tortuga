export class DetailedNote {
    constructor(
        public id: number,
        public title: string,
        public content: string,
        public version: number,
    ) { }
}
