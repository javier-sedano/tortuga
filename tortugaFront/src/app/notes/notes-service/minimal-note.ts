export class MinimalNote {
    constructor(
        public id: number,
        public title: string,
    ) { }
}
