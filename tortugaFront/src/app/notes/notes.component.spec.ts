import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { LoggerTestingModule } from 'ngx-logger/testing';
import { FormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { NGX_TRANSLATE_TESTING_CONF } from 'src/testing/testing.i18n';
import { NotesComponent } from './notes.component';
import { NotesService } from './notes-service/notes.service';
import { DetailedNote } from './notes-service/detailed-note';
import { MinimalNote } from './notes-service/minimal-note';

describe('NotesComponent', () => {
  let component: NotesComponent;
  let fixture: ComponentFixture<NotesComponent>;
  let notesServiceSpyObj;

  beforeEach(waitForAsync(() => {
    notesServiceSpyObj = jasmine.createSpyObj('NotesService', ['findNotes', 'getNote']);
    TestBed.configureTestingModule({
      declarations: [NotesComponent],
      imports: [
        LoggerTestingModule,
        RouterTestingModule,
        TranslateModule.forRoot(NGX_TRANSLATE_TESTING_CONF),
        FormsModule,
        InfiniteScrollModule,
        FontAwesomeModule,
      ],
      providers: [
        { provide: NotesService, useValue: notesServiceSpyObj },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotesComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    let foundNotes = Array();
    foundNotes.push(new MinimalNote(42, "answer"));
    foundNotes.push(new MinimalNote(2, "first prime"));
    notesServiceSpyObj.findNotes.and.returnValue(of(foundNotes));
    notesServiceSpyObj.getNote.and.returnValue(of(new DetailedNote(42, "answer", "to life", 1)));
    fixture.detectChanges();
    expect(component.notes).toEqual(foundNotes);
    expect(notesServiceSpyObj.findNotes).toHaveBeenCalledWith("", 0);
    expect(component.isFinding).toBeFalsy();
  });

  it('should toggle visibility of search panel', () => {
    expect(component.showSmallSearchPanel).toBeFalsy();
    component.toggleShowSmallSearchPanel();
    expect(component.showSmallSearchPanel).toBeTruthy();
    component.toggleShowSmallSearchPanel();
    expect(component.showSmallSearchPanel).toBeFalsy();
  });

});
