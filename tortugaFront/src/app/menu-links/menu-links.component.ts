import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { LoginService } from 'src/app/login/login.service';
import { Role } from 'src/app/login/role';

@Component({
  selector: 'app-menu-links',
  templateUrl: './menu-links.component.html',
})
export class MenuLinksComponent {
  Role = Role;

  constructor(
    private loginService: LoginService,
    private router: Router,
  ) { }

  logout() {
    this.loginService.logout(() => {
      this.router.navigateByUrl('/');
    });
  }

  getUsername() {
    return { username: this.loginService.getUsername() };
  }

  hasRole(role: Role): boolean {
    return this.loginService.hasRole(role);
  }

}
