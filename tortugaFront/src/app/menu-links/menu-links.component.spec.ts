import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { LoggerTestingModule } from 'ngx-logger/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ComponentFixture, TestBed, fakeAsync, waitForAsync } from '@angular/core/testing';

import { NGX_TRANSLATE_TESTING_CONF } from 'src/testing/testing.i18n';
import { MenuLinksComponent } from './menu-links.component';
import { LoginService } from '../login/login.service';

describe('MenuLinksComponent', () => {
  let component: MenuLinksComponent;
  let fixture: ComponentFixture<MenuLinksComponent>;
  let loginServiceSpyObj;

  beforeEach(waitForAsync(() => {
    loginServiceSpyObj = jasmine.createSpyObj('LoginService', ['logout', 'getUsername', 'hasRole']);
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        LoggerTestingModule,
        TranslateModule.forRoot(NGX_TRANSLATE_TESTING_CONF),
      ],
      declarations: [MenuLinksComponent],
      providers: [
        { provide: LoginService, useValue: loginServiceSpyObj },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuLinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    TestBed.get(TranslateService).use("l4ng");
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have filled logout when authenticated', fakeAsync(() => {
    loginServiceSpyObj.getUsername.and.returnValue("Thor");
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('#app-bar-logout').text).toEqual('L0g0ut Thor');
  }));

  it('should have notes link when USER', fakeAsync(() => {
    loginServiceSpyObj.getUsername.and.returnValue('Thor');
    loginServiceSpyObj.hasRole.and.callFake(function (role) {
      if (role === 'ROLE_ADMIN') {
        return false;
      } else if (role === 'ROLE_USER') {
        return true;
      }
    });
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('#app-bar-notes')).not.toBeNull();
    expect(fixture.nativeElement.querySelector('#app-bar-admin')).toBeNull();
  }));

  it('should have notes and admin links when ADMIN', fakeAsync(() => {
    loginServiceSpyObj.getUsername.and.returnValue('Thor');
    loginServiceSpyObj.hasRole.and.callFake(function (role) {
      if (role === 'ROLE_ADMIN') {
        return true;
      } else if (role === 'ROLE_USER') {
        return true;
      }
    });
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('#app-bar-notes')).not.toBeNull();
    expect(fixture.nativeElement.querySelector('#app-bar-admin')).not.toBeNull();
  }));

  it('should logout when called', () => {
    const app = fixture.debugElement.componentInstance;
    loginServiceSpyObj.logout.and.callFake(function (okCallback: () => void) {
      okCallback();
    });
    app.logout();
    expect(loginServiceSpyObj.logout).toHaveBeenCalled();
  });


});
