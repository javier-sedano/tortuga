import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PageNotFoundComponent } from './struct/page-not-found/page-not-found.component';
import { IsAdminGuard } from './struct/guard/is.admin.guard';
import { IsUserGuard } from './struct/guard/is.user.guard';

const routes: Routes = [
  {
    path: 'notes',
    redirectTo: 'notes/',
    pathMatch: 'full'
  },
  {
    path: 'notes',
    canActivate: [IsUserGuard],
    loadChildren: () => import('./notes/notes.module').then(m => m.NotesModule),
    runGuardsAndResolvers: 'always'
  },
  {
    path: 'admin',
    canActivate: [IsAdminGuard],
    loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule),
    runGuardsAndResolvers: 'always'
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
  },
  { path: '', redirectTo: 'notes/', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload', relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
