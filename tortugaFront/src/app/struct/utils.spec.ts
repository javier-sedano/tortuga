import { fakeAsync, tick } from '@angular/core/testing';
import { delayedFeedbackDo } from './utils';

describe('utils', () => {

  describe('delayedFeedbackDo', () => {

    it('should immediately run do and undo if delayed 0', fakeAsync(() => {
      let doCalled = false;
      let undoCalled = false;
      const nodelay = 0;
      let delayedFeedbackDoer = delayedFeedbackDo(
        () => {
          doCalled = true;
        },
        nodelay
      );
      expect(doCalled).toEqual(true);
      delayedFeedbackDoer.undo(
        () => {
          undoCalled = true;
        }
      );
      expect(undoCalled).toEqual(true);
    }));

    it('should run do and undo if not enough time has passed', fakeAsync(() => {
      let doCalled = false;
      let undoCalled = false;
      const delay = 10;
      let delayedFeedbackDoer = delayedFeedbackDo(
        () => {
          doCalled = true;
        },
        delay
      );
      expect(doCalled).toEqual(false);
      tick(delay + 1);
      expect(doCalled).toEqual(true);
      delayedFeedbackDoer.undo(
        () => {
          undoCalled = true;
        }
      );
      expect(undoCalled).toEqual(true);
    }));

    it('should not run do nor undo if not enough time has passed', fakeAsync(() => {
      let doCalled = false;
      let undoCalled = false;
      const delay = 10;
      let delayedFeedbackDoer = delayedFeedbackDo(
        () => {
          doCalled = true;
        },
        delay
      );
      expect(doCalled).toEqual(false);
      tick(delay - 1);
      expect(doCalled).toEqual(false);
      delayedFeedbackDoer.undo(
        () => {
          undoCalled = true;
        }
      );
      expect(undoCalled).toEqual(false);
      tick(2);
      expect(doCalled).toEqual(false);
      expect(undoCalled).toEqual(false);
    }));

  });

});
