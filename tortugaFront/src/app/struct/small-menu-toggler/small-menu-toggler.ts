export class SmallMenuToggler {

    hideSmallMenu: boolean = true;

    toggleSmallMenuVisibility() {
        this.hideSmallMenu = !this.hideSmallMenu;
    }
}
