import { SmallMenuToggler } from './small-menu-toggler';

describe('SmallMenuToggler', () => {
  it('should create an instance', () => {
    expect(new SmallMenuToggler()).toBeTruthy();
  });

  it('should toggle small menu visibility', () => {
    const app = new SmallMenuToggler();
    expect(app.hideSmallMenu).toBeTruthy();
    app.toggleSmallMenuVisibility();
    expect(app.hideSmallMenu).toBeFalsy();
    app.toggleSmallMenuVisibility();
    expect(app.hideSmallMenu).toBeTruthy();
  });

});
