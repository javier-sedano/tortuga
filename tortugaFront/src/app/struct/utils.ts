import { environment } from 'src/environments/environment';

export function delayedFeedbackDo(
    doer: () => void,
    delay: number = environment.delayedFeedbackTime
): any {
    let done: boolean = false;
    let undone: boolean = false;
    if (delay > 0) {
        setTimeout(function () {
            if (!undone) {
                doer();
            }
            done = true;
        }, delay);
    } else {
        doer();
        done = true;
    }
    return {
        undo: function (undoer: () => void) {
            if (done) {
                undoer();
            }
            undone = true;
        }
    };
}

