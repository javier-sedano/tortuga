import { Injectable } from '@angular/core';
import { Subject, Observable, ObservableInput } from 'rxjs';
import { startWith, debounceTime, switchMap } from 'rxjs/operators';
import { NGXLogger } from 'ngx-logger';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DebouncerProviderService {
  constructor(private logger: NGXLogger) { }

  buildDebouncer<T, R>(
    debouncerSubject: Subject<T>,
    startWithValue: T,
    switchMapper: (value: T, index: number) => ObservableInput<R>,
    debounceTimeValue: number = environment.debounceTime
  ): Observable<R> {
    if (debounceTimeValue > 0) {
      this.logger.debug("Creating debouncer with time:", debounceTimeValue);
      console.debug("with debouncer");
      return debouncerSubject.pipe(
        startWith(startWithValue),
        debounceTime(debounceTimeValue),
        switchMap(switchMapper)
      );
    } else {
      this.logger.warn("Creating debouncer without debouncer. If this is shown in production, please report a bug.");
      return debouncerSubject.pipe(
        startWith(startWithValue),
        switchMap(switchMapper)
      );
    }
  }
}