import { LoggerTestingModule } from 'ngx-logger/testing';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { Subject } from 'rxjs';

import { DebouncerProviderService } from './debouncer-provider.service';

describe('DebouncerProviderService', () => {

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        LoggerTestingModule,
      ],
    });
  }));

  it('should be created', () => {
    const service: DebouncerProviderService = TestBed.get(DebouncerProviderService);
    expect(service).toBeTruthy();
  });

  it('should at least return something', () => {
    const service: DebouncerProviderService = TestBed.get(DebouncerProviderService);
    expect(service.buildDebouncer<string, string>(new Subject<string>(), "", () => "", 0)).toBeTruthy();
    expect(service.buildDebouncer<string, string>(new Subject<string>(), "", () => "", 1)).toBeTruthy();
    expect(service.buildDebouncer<string, string>(new Subject<string>(), "", () => "")).toBeTruthy();
  });

});
