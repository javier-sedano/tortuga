import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { IsLoggedInGuard } from './is.logged.in.guard';
import { Role } from './../../login/role';

export function initLoginServiceSpyObj() {
  return jasmine.createSpyObj('LoginService', ['isAuthenticated', 'setRedirectUrl', 'hasRole']);
}

export function initRouterSpyObj() {
  return jasmine.createSpyObj('Router', ['navigateByUrl']);
}

export function testNotLogged(guard: IsLoggedInGuard, loginServiceSpyObj, routerSpyObj) {
  loginServiceSpyObj.isAuthenticated.and.returnValue(false);
  let canActivate: boolean = guard.canActivate({} as ActivatedRouteSnapshot, { url: 'asdf' } as RouterStateSnapshot);
  expect(canActivate).toBeFalsy();
  expect(loginServiceSpyObj.setRedirectUrl).toHaveBeenCalledWith('asdf');
  expect(routerSpyObj.navigateByUrl).toHaveBeenCalledWith('/login');
}

export function testDisallowWrongRole(guard: IsLoggedInGuard, roleToSayNo: Role, loginServiceSpyObj, routerSpyObj) {
  loginServiceSpyObj.isAuthenticated.and.returnValue(true);
  loginServiceSpyObj.hasRole.and.callFake((role: Role) => {
    return (roleToSayNo != role);
  });
  let canActivate: boolean =
    guard.canActivate({} as ActivatedRouteSnapshot, { url: 'asdf' } as RouterStateSnapshot);
  expect(canActivate).toBeFalsy();
  expect(routerSpyObj.navigateByUrl).toHaveBeenCalledWith('/');
}

export function testAllowRightRole(guard: IsLoggedInGuard, roleToSayYes: Role, loginServiceSpyObj) {
  loginServiceSpyObj.isAuthenticated.and.returnValue(true);
  loginServiceSpyObj.hasRole.and.callFake((role: Role) => {
    return (roleToSayYes == role);
  });
  let canActivate: boolean =
    guard.canActivate({} as ActivatedRouteSnapshot, { url: 'asdf' } as RouterStateSnapshot);
  expect(canActivate).toBeTruthy();
}

