import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { NGXLogger } from 'ngx-logger';

import { IsLoggedInGuard } from './is.logged.in.guard';
import { LoginService } from './../../login/login.service';
import { Role } from '../../login/role';

@Injectable({
    providedIn: 'root',
})
export class IsUserGuard extends IsLoggedInGuard {
    constructor(logger: NGXLogger, loginService: LoginService, router: Router) {
        super(logger, loginService, router);
    }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        return super.isLoggedInAndHasRole(state.url, Role.ROLE_USER);
    }

}