import { TestBed } from '@angular/core/testing';
import { LoggerTestingModule } from 'ngx-logger/testing';
import { Router } from '@angular/router';

import { IsAdminGuard } from "./is.admin.guard";
import { Role } from './../../login/role';
import { LoginService } from './../../login/login.service';
import { testNotLogged, testDisallowWrongRole, testAllowRightRole, initLoginServiceSpyObj, initRouterSpyObj } from './base.guard.spec';

describe('IsAdminGuard', () => {
  let loginServiceSpyObj;
  let routerSpyObj;

  beforeEach(() => {
    loginServiceSpyObj = initLoginServiceSpyObj();
    routerSpyObj = initRouterSpyObj();
    TestBed.configureTestingModule({
      imports: [
        LoggerTestingModule,
      ],
      providers: [
        { provide: LoginService, useValue: loginServiceSpyObj },
        { provide: Router, useValue: routerSpyObj },
      ],
    });
  });

  it('should be created', () => {
    const guard: IsAdminGuard = TestBed.get(IsAdminGuard);
    expect(guard).toBeTruthy();
  });

  it('should disallow not logged user', () => {
    const guard: IsAdminGuard = TestBed.get(IsAdminGuard);
    testNotLogged(guard, loginServiceSpyObj, routerSpyObj);
  });

  it('should disallow wrong role', () => {
    const guard: IsAdminGuard = TestBed.get(IsAdminGuard);
    testDisallowWrongRole(guard, Role.ROLE_ADMIN, loginServiceSpyObj, routerSpyObj);
  });

  it('should allow right role', () => {
    const guard: IsAdminGuard = TestBed.get(IsAdminGuard);
    testAllowRightRole(guard, Role.ROLE_ADMIN, loginServiceSpyObj);
  });

});
