import { TestBed } from '@angular/core/testing';
import { LoggerTestingModule } from 'ngx-logger/testing';
import { Router } from '@angular/router';

import { Role } from './../../login/role';
import { LoginService } from './../../login/login.service';
import { testNotLogged, testDisallowWrongRole, testAllowRightRole, initLoginServiceSpyObj, initRouterSpyObj } from './base.guard.spec';
import { IsUserGuard } from './is.user.guard';

describe('IsUserGuard', () => {
  let loginServiceSpyObj;
  let routerSpyObj;

  beforeEach(() => {
    loginServiceSpyObj = initLoginServiceSpyObj();
    routerSpyObj = initRouterSpyObj();
    TestBed.configureTestingModule({
      imports: [
        LoggerTestingModule,
      ],
      providers: [
        { provide: LoginService, useValue: loginServiceSpyObj },
        { provide: Router, useValue: routerSpyObj },
      ],
    });
  });

  it('should be created', () => {
    const guard: IsUserGuard = TestBed.get(IsUserGuard);
    expect(guard).toBeTruthy();
  });

  it('should disallow not logged user', () => {
    const guard: IsUserGuard = TestBed.get(IsUserGuard);
    testNotLogged(guard, loginServiceSpyObj, routerSpyObj);
  });

  it('should disallow wrong role', () => {
    const guard: IsUserGuard = TestBed.get(IsUserGuard);
    testDisallowWrongRole(guard, Role.ROLE_USER, loginServiceSpyObj, routerSpyObj);
  });

  it('should allow right role', () => {
    const guard: IsUserGuard = TestBed.get(IsUserGuard);
    testAllowRightRole(guard, Role.ROLE_USER, loginServiceSpyObj);
  });

});
