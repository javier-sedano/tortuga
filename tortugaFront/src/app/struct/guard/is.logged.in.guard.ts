import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { NGXLogger } from 'ngx-logger';

import { Role } from './../../login/role';
import { LoginService } from './../../login/login.service';

export abstract class IsLoggedInGuard implements CanActivate {
    static readonly LOGIN_URL = '/login';
    static readonly ROOT_URL = '/';

    constructor(protected logger: NGXLogger,
        protected loginService: LoginService,
        private router: Router) { }

    abstract canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean;

    protected isLoggedInAndHasRole(url: string, role: Role): boolean {
        this.logger.debug('Checking loggedIn for:', url, 'and role:', role.toString());
        if (!this.loginService.isAuthenticated()) {
            this.loginService.setRedirectUrl(url);
            this.logger.info('Not logged in, redirecting');
            this.router.navigateByUrl(IsLoggedInGuard.LOGIN_URL);
            return false;
        }
        if (!this.loginService.hasRole(role)) {
            this.logger.info('Not authorized, redirecting');
            this.router.navigateByUrl(IsLoggedInGuard.ROOT_URL);
            return false;
        }
        return true;
    }

}