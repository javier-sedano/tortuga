import { TestBed, fakeAsync, tick, ComponentFixture, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { LoggerTestingModule } from 'ngx-logger/testing';
import { TranslateModule } from '@ngx-translate/core';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { asyncData } from 'src/testing/async-observable-helpers';
import { NGX_TRANSLATE_TESTING_CONF } from 'src/testing/testing.i18n';
import { AppComponent } from './app.component';
import { AppService } from './app.service';
import { LoginService } from './login/login.service';

describe('AppComponent', () => {
  const THEME_STORAGE_KEY = 'theme';
  const DEFAULT_THEME = 'light-green';
  const THEME_PREFIX = 'assets/css/themes/w3-theme-';
  const THEME_SUFFIX = '.css';
  const LANG_STORAGE_KEY = 'lang';
  const DEFAULT_LANG = 'en';

  let appServiceSpyObj;
  let loginServiceSpyObj;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(waitForAsync(() => {
    appServiceSpyObj = jasmine.createSpyObj('AppService', ['getVersionObservable']);
    loginServiceSpyObj = jasmine.createSpyObj('LoginService', ['isAuthenticated', 'logout', 'getUsername', 'hasRole']);
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        LoggerTestingModule,
        TranslateModule.forRoot(NGX_TRANSLATE_TESTING_CONF),
        FontAwesomeModule,
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        { provide: AppService, useValue: appServiceSpyObj },
        { provide: LoginService, useValue: loginServiceSpyObj },
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(AppComponent);
  }));

  it('should create the app', () => {
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should not have bar when not authenticated', fakeAsync(() => {
    loginServiceSpyObj.isAuthenticated.and.returnValue(false);
    appServiceSpyObj.getVersionObservable.and.returnValue(asyncData('qwerty'));
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('#app-bar')).toBeNull();
  }));

  it('should have filled bar when authenticated with admin', fakeAsync(() => {
    loginServiceSpyObj.isAuthenticated.and.returnValue(true);
    loginServiceSpyObj.hasRole.and.returnValue(true);
    appServiceSpyObj.getVersionObservable.and.returnValue(asyncData('qwerty'));
    loginServiceSpyObj.getUsername.and.returnValue("Thor");
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('#app-bar')).not.toBeNull();
    expect(fixture.nativeElement.querySelector('#app-actuator')).not.toBeNull();
    let verImg: HTMLElement = fixture.nativeElement.querySelector('.ppa-version');
    expect(verImg.attributes['title'].value).toEqual('...');
    tick();
    fixture.detectChanges();
    expect(verImg.attributes['title'].value).toEqual('qwerty');
  }));

  it('should have filled bar when authenticated without admin', fakeAsync(() => {
    loginServiceSpyObj.isAuthenticated.and.returnValue(true);
    loginServiceSpyObj.hasRole.and.returnValue(false);
    appServiceSpyObj.getVersionObservable.and.returnValue(asyncData('qwerty'));
    loginServiceSpyObj.getUsername.and.returnValue("Thor");
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('#app-bar')).not.toBeNull();
    expect(fixture.nativeElement.querySelector('#app-actuator')).toBeNull();
    let verImg: HTMLElement = fixture.nativeElement.querySelector('.ppa-version');
    expect(verImg.attributes['title'].value).toEqual('...');
    tick();
    fixture.detectChanges();
    expect(verImg.attributes['title'].value).toEqual('qwerty');
  }));

  it('should have default theme and change it when requested', () => {
    const app = fixture.debugElement.componentInstance;
    loginServiceSpyObj.isAuthenticated.and.returnValue(true);
    appServiceSpyObj.getVersionObservable.and.returnValue(asyncData('qwerty'));
    loginServiceSpyObj.getUsername.and.returnValue("Thor");
    localStorage.removeItem(THEME_STORAGE_KEY);
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('#app-theme').attributes['href'].value).toEqual(THEME_PREFIX + DEFAULT_THEME + THEME_SUFFIX);
    const THEME2 = "black";
    app.setTheme(THEME2);
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('#app-theme').attributes['href'].value).toEqual(THEME_PREFIX + THEME2 + THEME_SUFFIX);
    expect(localStorage.getItem(THEME_STORAGE_KEY)).toEqual(THEME2);
    localStorage.removeItem(THEME_STORAGE_KEY);
  });

  it('should have default lang and change it when requested and reuse it on next creation', () => {
    const app = fixture.debugElement.componentInstance;
    loginServiceSpyObj.isAuthenticated.and.returnValue(true);
    appServiceSpyObj.getVersionObservable.and.returnValue(asyncData('qwerty'));
    loginServiceSpyObj.getUsername.and.returnValue("Thor");
    localStorage.removeItem(LANG_STORAGE_KEY);
    fixture.detectChanges();
    expect(app.translate.currentLang).toEqual(DEFAULT_LANG);
    const LANG2 = "es";
    app.setLang(LANG2);
    expect(app.translate.currentLang).toEqual(LANG2);
    expect(localStorage.getItem(LANG_STORAGE_KEY)).toEqual(LANG2);
    TestBed.createComponent(AppComponent);
    expect(app.translate.currentLang).toEqual(LANG2);
    expect(localStorage.getItem(LANG_STORAGE_KEY)).toEqual(LANG2);
    localStorage.removeItem(LANG_STORAGE_KEY);
  });

});
