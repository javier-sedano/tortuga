import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { NGXLogger } from 'ngx-logger';

import { BASE_REST_URL } from 'src/app/app.constants';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  private static readonly VERSION_URL: string = BASE_REST_URL + 'ping/ver';

  constructor(private logger: NGXLogger, private http: HttpClient) { }

  getVersionObservable(): Observable<string> {
    return this.http.get(AppService.VERSION_URL, { responseType: "text" });
  }
}
