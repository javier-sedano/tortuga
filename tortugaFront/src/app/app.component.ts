import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { startWith } from 'rxjs/operators';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { faMicrochip, faFlag, faPalette, faBars } from '@fortawesome/free-solid-svg-icons';

import { ACTUATOR_URL } from 'src/app/app.constants';
import { Role } from './login/role';
import { AppService } from './app.service';
import { LoginService } from './login/login.service';
import { SmallMenuToggler } from './struct/small-menu-toggler/small-menu-toggler';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent extends SmallMenuToggler implements OnInit {
  static readonly THEME_STORAGE_KEY = 'theme';
  static readonly DEFAULT_THEME = 'light-green';
  static readonly THEME_PREFIX = 'assets/css/themes/w3-theme-';
  static readonly THEME_SUFFIX = '.css';
  static readonly LANG_STORAGE_KEY = 'lang';
  static readonly DEFAULT_LANG = 'en';

  version$: Observable<string>;
  themeSanitizedUrl:SafeResourceUrl = this.getThemeSanitizedUrl();
  actuatorUrl: string = ACTUATOR_URL;
  Role = Role;
  faMicrochip = faMicrochip;
  faFlag = faFlag;
  faPalette = faPalette;
  faBars = faBars;

  constructor(
    private appService: AppService,
    private loginService: LoginService,
    private domSanitizer: DomSanitizer,
    private translate: TranslateService,
  ) {
    super();
    translate.setDefaultLang(AppComponent.DEFAULT_LANG);
    let lang: string = localStorage.getItem(AppComponent.LANG_STORAGE_KEY);
    if (lang == null) {
      lang = AppComponent.DEFAULT_LANG;
    }
    translate.use(lang);
  }

  ngOnInit() {
    this.version$ = this.appService.getVersionObservable().pipe(startWith('...'));
  }

  isAuthenticated(): boolean {
    return this.loginService.isAuthenticated();
  }

  hasRole(role: Role): boolean {
    return this.loginService.hasRole(role);
  }

  setTheme(theme: string) {
    localStorage.setItem(AppComponent.THEME_STORAGE_KEY, theme);
    this.themeSanitizedUrl = this.getThemeSanitizedUrl();
  }

  private getThemeSanitizedUrl(): SafeResourceUrl {
    let theme: string = localStorage.getItem(AppComponent.THEME_STORAGE_KEY);
    if (theme == null) {
      theme = AppComponent.DEFAULT_THEME;
    }
    return this.domSanitizer.bypassSecurityTrustResourceUrl(AppComponent.THEME_PREFIX + theme + AppComponent.THEME_SUFFIX);
  }

  setLang(lang: string) {
    localStorage.setItem(AppComponent.LANG_STORAGE_KEY, lang);
    this.translate.use(lang);
  }

}
