import { TranslateService } from '@ngx-translate/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NGXLogger } from 'ngx-logger';
import { ErrorDetails } from './error.details';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {
  private static readonly DEFAULT_I18N = "error.i18nCode.default";

  private static readonly DEFAULT_ERROR_MESSAGE = "There was an error, but I was not able to translate it. Please report a bug.";

  private errorMessage: string = undefined;

  constructor(
    private logger: NGXLogger,
    private translateService: TranslateService,
  ) { }

  getErrorMessage(): string {
    return this.errorMessage;
  }

  clearErrorMessage(): void {
    this.errorMessage = undefined;
  }

  handleError(err: any) {
    this.logger.error("ErrorService Error:", err);
    let errorDetails: ErrorDetails = new ErrorDetails();
    errorDetails.i18nCode = ErrorService.DEFAULT_I18N;
    errorDetails.args = { message: "" + err };
    if (err instanceof HttpErrorResponse) {
      errorDetails.args = { message: err.status + ": " + err.statusText };
      if (err.status === 400) {
        if ((err.error != null)
          && (err.error.i18nCode !== undefined)
          && (err.error.args !== undefined)) {
          errorDetails.i18nCode = err.error.i18nCode;
          errorDetails.args = err.error.args;
        }
      } else if (err.status === 401) {
        this.logger.debug("Error 401 ignored because it is handled elsewhere");
        return;
      }
    }
    this.logger.error("ErrorDetails:", errorDetails);
    this.translateService.get(errorDetails.i18nCode, errorDetails.args).subscribe( // NOSONAR
      value => {
        this.logger.debug("Translated error:", value);
        this.errorMessage = value;
      },
      error => {
        this.logger.fatal("Unable to resolve translation for error:", error, errorDetails);
        this.errorMessage = ErrorService.DEFAULT_ERROR_MESSAGE;
      }
    );
  }

}
