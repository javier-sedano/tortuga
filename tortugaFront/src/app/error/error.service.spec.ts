import { HttpErrorResponse } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { LoggerTestingModule } from 'ngx-logger/testing';
import { TestBed } from '@angular/core/testing';
import { of, throwError } from 'rxjs';

import { ErrorService } from './error.service';

describe('ErrorService', () => {
  const DEFAULT_I18N: string = "error.i18nCode.default";

  let translateServiceSpyObj;

  beforeEach(() => {
    translateServiceSpyObj = jasmine.createSpyObj('TranslateService', ['get']);
    TestBed.configureTestingModule({
      imports: [
        LoggerTestingModule,
      ],
      providers: [
        { provide: TranslateService, useValue: translateServiceSpyObj },
      ],
    });
  });

  it('should be created', () => {
    const service: ErrorService = TestBed.get(ErrorService);
    expect(service).toBeTruthy();
  });

  it('should show default message on non-http error', () => {
    const service: ErrorService = TestBed.get(ErrorService);
    const SHOWN_ERROR: string = "Un3xp3ct3d 3rr0r";
    const ERROR_CONTENT: string = "Error";
    let err: Error = new Error();
    translateServiceSpyObj.get.and.returnValue(of(SHOWN_ERROR));
    service.handleError(err);
    expect(translateServiceSpyObj.get).toHaveBeenCalledWith(DEFAULT_I18N, { message: ERROR_CONTENT });
    expect(service.getErrorMessage()).toEqual(SHOWN_ERROR);
  });

  it('should show status message on http non-400 error', () => {
    const service: ErrorService = TestBed.get(ErrorService);
    const SHOWN_ERROR: string = "Un3xp3ct3d 3rr0r";
    const ERROR_CONTENT = { status: 999, statusText: 'ups!' };
    let err: Error = new HttpErrorResponse(ERROR_CONTENT);
    translateServiceSpyObj.get.and.returnValue(of(SHOWN_ERROR));
    service.handleError(err);
    expect(translateServiceSpyObj.get).toHaveBeenCalledWith(DEFAULT_I18N, { message: ERROR_CONTENT.status + ": " + ERROR_CONTENT.statusText });
    expect(service.getErrorMessage()).toEqual(SHOWN_ERROR);
  });

  it('should ignore http 401 error because it is handled elsewhere', () => {
    const service: ErrorService = TestBed.get(ErrorService);
    const ERROR_CONTENT = { status: 401, statusText: 'Unauthorized' };
    let err: Error = new HttpErrorResponse(ERROR_CONTENT);
    service.handleError(err);
    expect(translateServiceSpyObj.get).not.toHaveBeenCalled();
    expect(service.getErrorMessage()).toEqual(undefined);
  });

  it('should show default message on http 400 error with wrong content (error)', () => {
    const service: ErrorService = TestBed.get(ErrorService);
    const SHOWN_ERROR: string = "Un3xp3ct3d 3rr0r";
    const I18NCODE: string = "a.given.code";
    const ARGS = { one: "1" };
    const ERROR_CONTENT = { status: 400, statusText: 'ups!', Xerror: { i18nCode: I18NCODE, args: ARGS } };
    let err: Error = new HttpErrorResponse(ERROR_CONTENT);
    translateServiceSpyObj.get.and.returnValue(of(SHOWN_ERROR));
    service.handleError(err);
    expect(translateServiceSpyObj.get).toHaveBeenCalledWith(DEFAULT_I18N, { message: ERROR_CONTENT.status + ": " + ERROR_CONTENT.statusText });
    expect(service.getErrorMessage()).toEqual(SHOWN_ERROR);
  });

  it('should show default message on http 400 error with wrong content (error.i18nCode)', () => {
    const service: ErrorService = TestBed.get(ErrorService);
    const SHOWN_ERROR: string = "Un3xp3ct3d 3rr0r";
    const I18NCODE: string = "a.given.code";
    const ARGS = { one: "1" };
    const ERROR_CONTENT = { status: 400, statusText: 'ups!', error: { Xi18nCode: I18NCODE, args: ARGS } };
    let err: Error = new HttpErrorResponse(ERROR_CONTENT);
    translateServiceSpyObj.get.and.returnValue(of(SHOWN_ERROR));
    service.handleError(err);
    expect(translateServiceSpyObj.get).toHaveBeenCalledWith(DEFAULT_I18N, { message: ERROR_CONTENT.status + ": " + ERROR_CONTENT.statusText });
    expect(service.getErrorMessage()).toEqual(SHOWN_ERROR);
  });

  it('should show default message on http 400 error with wrong content (error.args)', () => {
    const service: ErrorService = TestBed.get(ErrorService);
    const SHOWN_ERROR: string = "Un3xp3ct3d 3rr0r";
    const I18NCODE: string = "a.given.code";
    const ARGS = { one: "1" };
    const ERROR_CONTENT = { status: 400, statusText: 'ups!', error: { i18nCode: I18NCODE, Xargs: ARGS } };
    let err: Error = new HttpErrorResponse(ERROR_CONTENT);
    translateServiceSpyObj.get.and.returnValue(of(SHOWN_ERROR));
    service.handleError(err);
    expect(translateServiceSpyObj.get).toHaveBeenCalledWith(DEFAULT_I18N, { message: ERROR_CONTENT.status + ": " + ERROR_CONTENT.statusText });
    expect(service.getErrorMessage()).toEqual(SHOWN_ERROR);
  });

  it('should show i18n message on http 400 error with right content', () => {
    const service: ErrorService = TestBed.get(ErrorService);
    const SHOWN_ERROR: string = "3xp3ct3d 3rr0r";
    const I18NCODE: string = "a.given.code";
    const ARGS = { one: "1" };
    const ERROR_CONTENT = { status: 400, statusText: 'ups!', error: { i18nCode: I18NCODE, args: ARGS } };
    let err: Error = new HttpErrorResponse(ERROR_CONTENT);
    translateServiceSpyObj.get.and.returnValue(of(SHOWN_ERROR));
    service.handleError(err);
    expect(translateServiceSpyObj.get).toHaveBeenCalledWith(I18NCODE, ARGS);
    expect(service.getErrorMessage()).toEqual(SHOWN_ERROR);
  });

  it('should show something if translation fails', () => {
    const service: ErrorService = TestBed.get(ErrorService);
    const I18NCODE: string = "a.given.code";
    const ARGS = { one: "1" };
    const ERROR_CONTENT = { status: 400, statusText: 'ups!', error: { i18nCode: I18NCODE, args: ARGS } };
    let err: Error = new HttpErrorResponse(ERROR_CONTENT);
    translateServiceSpyObj.get.and.callFake(() => {
      return throwError("Unable to translate");
    });
    service.handleError(err);
    expect(translateServiceSpyObj.get).toHaveBeenCalledWith(I18NCODE, ARGS);
    expect(service.getErrorMessage()).not.toBeFalsy();
  });

});
