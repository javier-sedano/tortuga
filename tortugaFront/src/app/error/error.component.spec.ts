import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { LoggerTestingModule } from 'ngx-logger/testing';
import { TranslateModule } from '@ngx-translate/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { NGX_TRANSLATE_TESTING_CONF } from 'src/testing/testing.i18n';
import { ErrorComponent } from './error.component';
import { ErrorService } from './error.service';

describe('ErrorComponent', () => {
  let component: ErrorComponent;
  let fixture: ComponentFixture<ErrorComponent>;
  let errorServiceSpyObj;

  beforeEach(waitForAsync(() => {
    errorServiceSpyObj = jasmine.createSpyObj('ErrorService', ['getErrorMessage', 'clearErrorMessage']);
    TestBed.configureTestingModule({
      imports: [
        LoggerTestingModule,
        TranslateModule.forRoot(NGX_TRANSLATE_TESTING_CONF),
        FontAwesomeModule,
      ],
      declarations: [ErrorComponent],
      providers: [
        { provide: ErrorService, useValue: errorServiceSpyObj },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
