import { Component } from '@angular/core';
import { NGXLogger } from 'ngx-logger';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';

import { ErrorService } from './error.service';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
})
export class ErrorComponent {
  faExclamationTriangle = faExclamationTriangle;

  constructor(private logger: NGXLogger, private errorService: ErrorService) { }

  getErrorMessage(): string {
    return this.errorService.getErrorMessage();
  }

  dismissError(): void {
    this.errorService.clearErrorMessage();
  }

}
