import { Component, OnInit } from '@angular/core';
import { NGXLogger } from 'ngx-logger';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { startWith } from 'rxjs/operators';
import { faUser, faLock } from '@fortawesome/free-solid-svg-icons';

import { AppService } from '../app.service';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  username: string = '';
  password: string = '';
  failed: boolean = false;
  versionObservable: Observable<string>;
  isLogining: boolean = false;
  faUser = faUser;
  faLock = faLock;

  constructor(private logger: NGXLogger,
    private loginService: LoginService,
    private appService: AppService,
    private router: Router) { }

  ngOnInit() {
    this.loginService.clearLoggedUser();
    this.versionObservable = this.appService.getVersionObservable().pipe(startWith('...'));
  }

  login() {
    this.logger.debug("Logging in:", this.username, "****");
    this.failed = false;
    this.isLogining = true;
    this.loginService.login(this.username, this.password,
      () => { this.onLoginSucess() }, () => { this.onLoginError() });
  }

  onLoginSucess() {
    this.logger.info("Logged in:", this.username);
    this.failed = false;
    this.isLogining = false;
    this.router.navigateByUrl(this.loginService.getRedirectUrl());
  }

  onLoginError() {
    this.logger.error("Authentication failure");
    this.failed = true;
    this.isLogining = false;
  }
}
