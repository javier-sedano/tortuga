import { ComponentFixture, TestBed, fakeAsync, tick, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing'
import { LoggerTestingModule } from 'ngx-logger/testing';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { NGX_TRANSLATE_TESTING_CONF } from 'src/testing/testing.i18n';
import { asyncData } from 'src/testing/async-observable-helpers';
import { LoginComponent } from './login.component';
import { AppService } from '../app.service';
import { LoginService } from './login.service';

describe('LoginComponent', () => {
  const AN_URL:string = '/qwertyuiop';

  let appServiceSpyObj;
  let loginServiceSpyObj;
  let routerSpyObj;
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(waitForAsync(() => {
    appServiceSpyObj = jasmine.createSpyObj('AppService', ['getVersionObservable']);
    loginServiceSpyObj = jasmine.createSpyObj('LoginService', ['login', 'getRedirectUrl', 'clearLoggedUser']);
    routerSpyObj = jasmine.createSpyObj('Router', ['navigateByUrl']);
    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot(NGX_TRANSLATE_TESTING_CONF),
        LoggerTestingModule,
        FormsModule,
        FontAwesomeModule,
      ],
      providers: [
        { provide: AppService, useValue: appServiceSpyObj },
        { provide: LoginService, useValue: loginServiceSpyObj },
        { provide: Router, useValue: routerSpyObj },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have about with version', fakeAsync(() => {
    appServiceSpyObj.getVersionObservable.and.returnValue(asyncData('qwerty'));
    fixture.detectChanges();
    expect(loginServiceSpyObj.clearLoggedUser).toHaveBeenCalled();
    let aboutDiv: HTMLElement = fixture.nativeElement.querySelector('#login-about');
    expect(aboutDiv.textContent).toContain('...');
    tick();
    fixture.detectChanges();
    expect(aboutDiv.textContent).toContain('qwerty');
  }));

  function testLoginWithRightCredentials() {
    const loginComponent = fixture.debugElement.componentInstance;
    loginServiceSpyObj.login.and.callFake(
      function (username: string, password: string, okCallback:() => void, errorCallback:() => void) {
        expect(loginComponent.failed).toEqual(false);
        expect(loginComponent.isLogining).toEqual(true);
        okCallback();
      }
    );
    loginServiceSpyObj.getRedirectUrl.and.returnValue(AN_URL);
    expect(loginComponent.isLogining).toEqual(false);
    loginComponent.login();
    expect(loginComponent.isLogining).toEqual(false);
    expect(loginComponent.failed).toEqual(false);
    expect(routerSpyObj.navigateByUrl).toHaveBeenCalledWith(AN_URL);
  }

  it('should login when right credentials', () => {
    testLoginWithRightCredentials();
  });

  function testLoginWithWrongCredentials() {
    const loginComponent = fixture.debugElement.componentInstance;
    loginServiceSpyObj.login.and.callFake(
      function (username: string, password: string, okCallback:() => void, errorCallback:() => void) {
        expect(loginComponent.isLogining).toEqual(true);
        errorCallback();
      }
    );
    loginServiceSpyObj.getRedirectUrl.and.returnValue(AN_URL);
    expect(loginComponent.isLogining).toEqual(false);
    loginComponent.login();
    expect(loginComponent.isLogining).toEqual(false);
    expect(loginComponent.failed).toEqual(true);
    expect(routerSpyObj.navigateByUrl).not.toHaveBeenCalled();

  }

  it('should not login when wrong credentials', () => {
    testLoginWithWrongCredentials();
  });

  it('should login with right credentials after wrong credentials', () => {
    testLoginWithWrongCredentials();
    testLoginWithRightCredentials();
  });

});
