import { Injectable } from '@angular/core';
import { NGXLogger } from 'ngx-logger';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';

import { IsLoggedInGuard } from '../struct/guard/is.logged.in.guard';

@Injectable({
  providedIn: 'root'
})
export class LoginInterceptor implements HttpInterceptor {

  constructor(private logger: NGXLogger, private router: Router) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(
        tap(
          () => {
            // Intentionally blank, nothing to do on success
          },
          (err: any) => {
            if (err instanceof HttpErrorResponse) {
              if (err.status === 401) {
                this.logger.error("LoginInterceptor Error:", err);
                this.router.navigateByUrl(IsLoggedInGuard.LOGIN_URL);
              }
            }
          })
      );
  }
}
