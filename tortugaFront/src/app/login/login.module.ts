import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { NGX_TRANSLATE_CONF } from 'src/app/app.module';
import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';

@NgModule({
  declarations: [
    LoginComponent,
  ],
  imports: [
    CommonModule,
    TranslateModule.forChild(NGX_TRANSLATE_CONF),
    FormsModule,
    LoginRoutingModule,
    FontAwesomeModule,
  ]
})
export class LoginModule { }
