export class LoggedUser {
    static readonly VERSION: number = 2;
    username: string;
    roles: string[];
}
