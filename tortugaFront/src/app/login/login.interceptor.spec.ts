import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { LoggerTestingModule } from 'ngx-logger/testing';
import { HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { of, throwError } from 'rxjs';

import { LoginInterceptor } from './login.interceptor';

describe('LoginInterceptor', () => {
  const LOGIN_URL: string = '/login';

  let routerSpyObj;
  let httpHandlerSpyObj;

  beforeEach(() => {
    routerSpyObj = jasmine.createSpyObj('Router', ['navigateByUrl']);
    httpHandlerSpyObj = jasmine.createSpyObj('HttpHandler', ['handle']);
    TestBed.configureTestingModule({
      imports: [
        LoggerTestingModule,
      ],
      providers: [
        { provide: Router, useValue: routerSpyObj },
      ],
    });
  });

  it('should be created', () => {
    const interceptor: LoginInterceptor = TestBed.get(LoginInterceptor);
    expect(interceptor).toBeTruthy();
  });

  it('should pass-through success', () => {
    const interceptor: LoginInterceptor = TestBed.get(LoginInterceptor);
    const HTTP_EVENT: HttpEvent<any> = new HttpResponse<string>();
    httpHandlerSpyObj.handle.and.returnValue(of(HTTP_EVENT));
    interceptor.intercept(null, httpHandlerSpyObj).subscribe(
      value => {
        expect(value).toBe(HTTP_EVENT);
        expect(routerSpyObj.navigateByUrl).not.toHaveBeenCalled();
      },
      fail
    );
  });

  it('should not redirect non-http error', () => {
    const interceptor: LoginInterceptor = TestBed.get(LoginInterceptor);
    httpHandlerSpyObj.handle.and.callFake(() => {
      return throwError("Error");
    });
    interceptor.intercept(null, httpHandlerSpyObj).subscribe(
      fail,
      error => {
        expect(error).toEqual("Error");
        expect(routerSpyObj.navigateByUrl).not.toHaveBeenCalled();
      }
    );
  });

  it('should not redirect on http non-401 error', () => {
    const interceptor: LoginInterceptor = TestBed.get(LoginInterceptor);
    const HTTP_ERROR_RESPONSE: HttpErrorResponse = new HttpErrorResponse({ status: 500 });
    httpHandlerSpyObj.handle.and.callFake(() => {
      return throwError(HTTP_ERROR_RESPONSE);
    });
    interceptor.intercept(null, httpHandlerSpyObj).subscribe(
      fail,
      error => {
        expect(error).toBe(HTTP_ERROR_RESPONSE);
        expect(routerSpyObj.navigateByUrl).not.toHaveBeenCalled();
      }
    );
  });

  it('should redirect on http 401 error', () => {
    const interceptor: LoginInterceptor = TestBed.get(LoginInterceptor);
    const HTTP_ERROR_RESPONSE: HttpErrorResponse = new HttpErrorResponse({ status: 401 });
    httpHandlerSpyObj.handle.and.callFake(() => {
      return throwError(HTTP_ERROR_RESPONSE);
    });
    interceptor.intercept(null, httpHandlerSpyObj).subscribe(
      fail,
      error => {
        expect(error).toBe(HTTP_ERROR_RESPONSE);
        expect(routerSpyObj.navigateByUrl).toHaveBeenCalledWith(LOGIN_URL);
      }
    );
  });

});
