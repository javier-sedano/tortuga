import { TestBed } from '@angular/core/testing';
import { LoggerTestingModule } from 'ngx-logger/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { BASE_REST_URL } from 'src/testing/testing.constants';
import { LoginService } from './login.service';
import { LoggedUser } from './logged.user';
import { Role } from './role';

describe('LoginService', () => {
  const LOGGED_USER_LOCALSTORAGE_KEY: string = 'loggedUser';
  const LOGIN_URL = BASE_REST_URL + 'login';
  const LOGOUT_URL = BASE_REST_URL + 'logout';

  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        LoggerTestingModule,
      ],
    });
    httpTestingController = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
    localStorage.removeItem(LOGGED_USER_LOCALSTORAGE_KEY);
  });

  it('should be created', () => {
    const service: LoginService = TestBed.get(LoginService);
    expect(service).toBeTruthy();
  });

  it('should store redirectUrl', () => {
    const service: LoginService = TestBed.get(LoginService);
    service.setRedirectUrl('qwertyuiop');
    expect(service.getRedirectUrl()).toEqual('qwertyuiop');
  });

  function testCorrectLogout(service) {
    service.logout(() => {
      expect(service.isAuthenticated()).toBeFalsy();
      expect(service.getUsername()).toBeFalsy();
      expect(service.getRedirectUrl()).toEqual('/');
    });
    const req = httpTestingController.expectOne(LOGOUT_URL);
    expect(req.request.method).toEqual('POST');
    req.flush("");
  }

  function testIncorrectLogout(service) {
    service.logout(() => {
      expect(service.isAuthenticated()).toBeTruthy();
    });
    const req = httpTestingController.expectOne(LOGOUT_URL);
    expect(req.request.method).toEqual('POST');
    req.flush("", { status: 500, statusText: 'Unexpected error' });
  }

  function testLogin(service, okFollowUp) {
    service.login('one', '0n3', () => {
      expect(service.isAuthenticated()).toBeTruthy();
      expect(service.getUsername()).toEqual('one');
      expect(service.hasRole(Role.ROLE_USER)).toBeTruthy();
      expect(service.hasRole(Role.ROLE_ADMIN)).toBeFalsy();
      okFollowUp();
    }, () => {
      fail();
    });
    const req = httpTestingController.expectOne(LOGIN_URL);
    expect(req.request.method).toEqual('POST');
    let returnedUser: LoggedUser = new LoggedUser();
    returnedUser.username = 'one';
    returnedUser.roles = [Role.ROLE_USER.toString()];
    req.flush(returnedUser);
  }

  it('should be able to login and logout', () => {
    const service: LoginService = TestBed.get(LoginService);
    testLogin(service, () => {
      testCorrectLogout(service);
    });
  });

  it('should be able to login and logout', () => {
    const service: LoginService = TestBed.get(LoginService);
    testLogin(service, () => {
      testIncorrectLogout(service);
    });
  });

  it('should detect login failure', () => {
    const service: LoginService = TestBed.get(LoginService);
    service.login('one', '0n3', () => {
      fail();
    }, () => {
      expect(service.isAuthenticated()).toBeFalsy();
    });
    const req = httpTestingController.expectOne(LOGIN_URL);
    expect(req.request.method).toEqual('POST');
    req.flush("", { status: 401, statusText: 'Not authorized' });
  });

  it('should not have stored loggedUser by default', () => {
    const service: LoginService = TestBed.get(LoginService);
    localStorage.removeItem(LOGGED_USER_LOCALSTORAGE_KEY);
    expect(service.isAuthenticated()).toBeFalsy();
  });

  function testStoredLoggedUser(service: LoginService) {
    let storedLoggedUser: any = {
      username: 'two',
      roles: ['ROLE_USER'],
      version: LoggedUser.VERSION,
    };
    let storedLoggedUserJson: string = JSON.stringify(storedLoggedUser);
    localStorage.setItem(LOGGED_USER_LOCALSTORAGE_KEY, storedLoggedUserJson);
    expect(service.isAuthenticated()).toBeTruthy();
    expect(service.getUsername()).toEqual('two');
    expect(service.hasRole(Role.ROLE_USER)).toBeTruthy();
    expect(service.hasRole(Role.ROLE_ADMIN)).toBeFalsy();
  }

  it('should use stored loggedUser if available', () => {
    const service: LoginService = TestBed.get(LoginService);
    testStoredLoggedUser(service);
  });

  function testWronglyStoredLoggedUser(service: LoginService, storedLoggedUser: any) {
    let storedLoggedUserJson: string = JSON.stringify(storedLoggedUser);
    localStorage.setItem(LOGGED_USER_LOCALSTORAGE_KEY, storedLoggedUserJson);
    expect(service.isAuthenticated()).toBeFalsy();
  }

  it('should not use wrongly stored (without version) loggedUser', () => {
    const service: LoginService = TestBed.get(LoginService);
    let storedLoggedUser: any = {
      username: 'two',
      roles: ['ROLE_USER'],
    };
    testWronglyStoredLoggedUser(service, storedLoggedUser);
  });

  it('should not use wrongly stored (bad version) loggedUser', () => {
    const service: LoginService = TestBed.get(LoginService);
    let storedLoggedUser: any = {
      username: 'two',
      roles: ['ROLE_USER'],
      version: (LoggedUser.VERSION - 1),
    };
    testWronglyStoredLoggedUser(service, storedLoggedUser);
  });

  it('should clean storedLogged user when requested', () => {
    const service: LoginService = TestBed.get(LoginService);
    testStoredLoggedUser(service);
    service.clearLoggedUser();
    expect(service.isAuthenticated()).toBeFalsy();
  });

});
