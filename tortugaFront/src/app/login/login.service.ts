import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { NGXLogger } from 'ngx-logger';

import { BASE_REST_URL } from 'src/app/app.constants';import { LoggedUser } from './logged.user';
import { Role } from './role';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private static readonly LOGIN_URL: string = BASE_REST_URL + 'login';
  private static readonly LOGOUT_URL: string = BASE_REST_URL + 'logout';
  private static readonly LOGGED_USER_LOCALSTORAGE_KEY: string = 'loggedUser';

  private redirectUrl: string = '/';

  constructor(private logger: NGXLogger, private http: HttpClient) { }

  login(username: string, password: string, okCallback: () => void, errorCallback: () => void) {
    const loginParams: HttpParams = new HttpParams()
      .set('username', username)
      .set('password', password);
    this.http
      .post<LoggedUser>(LoginService.LOGIN_URL, loginParams)
      .subscribe( // NOSONAR
        (loggedUser: LoggedUser) => { this.onAuthentication(loggedUser, okCallback) },
        (error) => { this.onAuthenticationError(error, errorCallback); }
      );
  }

  private onAuthentication(loggedUser: LoggedUser, okCallback: () => void) {
    this.logger.debug("Received loggedUser:", loggedUser);
    this.logger.debug(JSON.stringify(loggedUser));
    this.setLoggedUser(loggedUser);
    okCallback();
  }

  private onAuthenticationError(error: any, errorCallback: () => void) {
    this.logger.debug(error);
    this.clearLoggedUser();
    errorCallback();
  }

  logout(okCallback: () => void) {
    this.logger.info("Logging out");
    this.http
      .post<string>(LoginService.LOGOUT_URL, null)
      .subscribe( // NOSONAR
        () => { this.onLogout(okCallback) },
        (error) => { this.onLogoutError(error); }
      );
  }

  private onLogout(okCallback: () => void) {
    this.clearLoggedUser();
    this.redirectUrl = '/';
    okCallback();
  }

  private onLogoutError(error: any) {
    this.logger.debug(error);
  }

  isAuthenticated(): boolean {
    return (this.getLoggedUser() != undefined);
  }

  getUsername(): string {
    return (this.isAuthenticated() ? this.getLoggedUser().username : undefined);
  }

  hasRole(role: Role): boolean {
    return this.isAuthenticated() && this.getLoggedUser().roles.includes(role.toString());
  }

  setRedirectUrl(url: string) {
    this.redirectUrl = url;
  }

  getRedirectUrl(): string {
    return this.redirectUrl;
  }

  private getLoggedUser(): LoggedUser {
    let loggedUserJson: string = localStorage.getItem(LoginService.LOGGED_USER_LOCALSTORAGE_KEY);
    if (loggedUserJson === null) {
      return undefined;
    }
    let loggedUser: any = JSON.parse(loggedUserJson);
    if ((loggedUser.version === undefined) || (loggedUser.version !== LoggedUser.VERSION)) {
      this.logger.warn("Discarding invalid loggedUser data:", loggedUser);
      return undefined;
    }
    return loggedUser;
  }

  clearLoggedUser() {
    localStorage.removeItem(LoginService.LOGGED_USER_LOCALSTORAGE_KEY);
  }

  private setLoggedUser(loggedUser: LoggedUser) {
    let storedLoggedUser: any = {
      ...loggedUser,
      version: LoggedUser.VERSION,
    }
    localStorage.setItem(LoginService.LOGGED_USER_LOCALSTORAGE_KEY, JSON.stringify(storedLoggedUser));
  }
}
