import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { LoggerModule } from 'ngx-logger';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { MultiTranslateHttpLoader } from "ngx-translate-multi-http-loader";
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { environment } from 'src/environments/environment';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { PageNotFoundComponent } from './struct/page-not-found/page-not-found.component';
import { LoginInterceptor } from './login/login.interceptor';
import { ErrorComponent } from './error/error.component';
import { MenuLinksComponent } from './menu-links/menu-links.component';

export function HttpLoaderFactory(http: HttpClient) {
  const I18N_PATH = "./assets/i18n/";
  return new MultiTranslateHttpLoader(http, [
    { prefix: I18N_PATH, suffix: ".json" },
    { prefix: I18N_PATH + "app/", suffix: ".json" },
    { prefix: I18N_PATH + "error/", suffix: ".json" },
    { prefix: I18N_PATH + "login/", suffix: ".json" },
    { prefix: I18N_PATH + "notes/", suffix: ".json" },
    { prefix: I18N_PATH + "admin/", suffix: ".json" },
    { prefix: I18N_PATH + "admin/system/", suffix: ".json" },
    { prefix: I18N_PATH + "admin/users/", suffix: ".json" },
  ]);
}

export const NGX_TRANSLATE_CONF = {
  loader: {
    provide: TranslateLoader,
    useFactory: HttpLoaderFactory,
    deps: [HttpClient]
  }
};

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    ErrorComponent,
    MenuLinksComponent,
  ],
  imports: [
    LoggerModule.forRoot({
      level: environment.ngxLogLevel
    }),
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    TranslateModule.forRoot(NGX_TRANSLATE_CONF),
    FontAwesomeModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoginInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
