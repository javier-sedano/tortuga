import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { LoggerTestingModule } from 'ngx-logger/testing';

import { BASE_REST_URL } from 'src/testing/testing.constants';
import { AppService } from './app.service';

describe('AppService', () => {

  const VERSION_URL: string = BASE_REST_URL + 'ping/ver';

  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        LoggerTestingModule,
      ],
    });
    httpTestingController = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    const service: AppService = TestBed.get(AppService);
    expect(service).toBeTruthy();
  });

  it('should return the version from the server', () => {
    const service: AppService = TestBed.get(AppService);
    service.getVersionObservable().subscribe(
      value => {
        expect(value).toEqual("test-1.2.3");
      },
      fail);
    const req = httpTestingController.expectOne(VERSION_URL);
    expect(req.request.method).toEqual('GET');
    req.flush('test-1.2.3');
  });

  it('should return the error from the server', () => {
    const service: AppService = TestBed.get(AppService);
    service.getVersionObservable().subscribe(
      fail,
      value => {
        expect(value.error).toEqual("Mistaken error");
        expect(value.status).toEqual(500);
        expect(value.statusText).toEqual("Internal");
      });
    const req = httpTestingController.expectOne(VERSION_URL);
    expect(req.request.method).toEqual('GET');
    req.flush('Mistaken error', { status: 500, statusText: 'Internal' });
  });

});
