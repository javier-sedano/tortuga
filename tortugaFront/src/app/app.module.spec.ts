import { TestBed } from '@angular/core/testing';
import { MultiTranslateHttpLoader } from 'ngx-translate-multi-http-loader';

import { NGX_TRANSLATE_CONF } from './app.module';

describe('AppModule', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
      ],
    });
  });

  afterEach(() => {
  });

  it('should have a configuration with a factory returning a TranslateHttpLoader', () => {
    const conf = NGX_TRANSLATE_CONF;
    expect(conf).not.toBeUndefined();
    expect(conf.loader).not.toBeUndefined();
    expect(conf.loader.useFactory).not.toBeUndefined();
    const translateHttpLoader = conf.loader.useFactory(null);
    if (!(translateHttpLoader instanceof MultiTranslateHttpLoader)) {
      fail();
    }
  });


});
