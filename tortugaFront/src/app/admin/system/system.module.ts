import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { NGX_TRANSLATE_CONF } from 'src/app/app.module';
import { SystemComponent } from './system.component';
import { SystemRoutingModule } from './system-routing.module';

@NgModule({
  declarations: [
    SystemComponent,
  ],
  imports: [
    CommonModule,
    TranslateModule.forChild(NGX_TRANSLATE_CONF),
    SystemRoutingModule,
  ]
})
export class SystemModule { }
