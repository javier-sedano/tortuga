import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { NGX_TRANSLATE_TESTING_CONF } from 'src/testing/testing.i18n';
import { AdminMenuLinksComponent } from './admin-menu-links.component';

describe('MenuLinksComponent', () => {
  let component: AdminMenuLinksComponent;
  let fixture: ComponentFixture<AdminMenuLinksComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot(NGX_TRANSLATE_TESTING_CONF),
      ],
      declarations: [ AdminMenuLinksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminMenuLinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
