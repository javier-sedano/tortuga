import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { NGX_TRANSLATE_CONF } from 'src/app/app.module';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { SystemModule } from './system/system.module';
import { UsersModule } from './users/users.module';
import { AdminMenuLinksComponent } from './admin-menu-links/admin-menu-links.component';

@NgModule({
  declarations: [
    AdminComponent,
    AdminMenuLinksComponent,
  ],
  imports: [
    CommonModule,
    TranslateModule.forChild(NGX_TRANSLATE_CONF),
    AdminRoutingModule,
    SystemModule,
    UsersModule,
    FontAwesomeModule,
  ]
})
export class AdminModule { }
