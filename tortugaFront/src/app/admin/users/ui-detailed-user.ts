import { Builder } from 'builder-pattern';
import { DetailedUser } from './users-service/detailed-user';

export class UiDetailedUser {
    public id: number;
    public username: string;
    public hash: string;
    public hashConfirmation: string;
    public enabled: boolean;
    public name: string;
    public version: number;
    public admin: boolean;

    static fromDetailedUser(detailedUser: DetailedUser): UiDetailedUser {
        return Builder(UiDetailedUser)
            .id(detailedUser.id)
            .username(detailedUser.username)
            .hash(detailedUser.hash)
            .hashConfirmation(detailedUser.hash)
            .enabled(detailedUser.enabled)
            .name(detailedUser.name)
            .version(detailedUser.version)
            .admin(detailedUser.admin)
            .build();
    }

    toDetailedUser(): DetailedUser {
        if (this.hash !== this.hashConfirmation) {
            throw new Error("Passwords do not match");
        }
        return Builder(DetailedUser)
            .id(this.id)
            .username(this.username)
            .hash(this.hash)
            .enabled(this.enabled)
            .name(this.name)
            .version(this.version)
            .admin(this.admin)
            .build();
    }

    passwordsMatch(): boolean {
        return this.hash === this.hashConfirmation;
    }
}
