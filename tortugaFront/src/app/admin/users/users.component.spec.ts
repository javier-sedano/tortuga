import { of } from 'rxjs';
import { Builder } from 'builder-pattern';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { LoggerTestingModule } from 'ngx-logger/testing';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { RouterTestingModule } from '@angular/router/testing';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { NGX_TRANSLATE_TESTING_CONF } from 'src/testing/testing.i18n';
import { UsersComponent } from './users.component';
import { UsersService } from './users-service/users.service';
import { MinimalUser } from './users-service/minimal-user';
import { DetailedUser } from './users-service/detailed-user';

describe('UsersComponent', () => {
  let component: UsersComponent;
  let fixture: ComponentFixture<UsersComponent>;
  let usersServiceSpyObj;

  beforeEach(waitForAsync(() => {
    usersServiceSpyObj = jasmine.createSpyObj('UsersService', ['findUsers', 'getUser']);
    TestBed.configureTestingModule({
      declarations: [UsersComponent],
      imports: [
        LoggerTestingModule,
        RouterTestingModule,
        TranslateModule.forRoot(NGX_TRANSLATE_TESTING_CONF),
        FormsModule,
        InfiniteScrollModule,
        FontAwesomeModule,
      ],
      providers: [
        { provide: UsersService, useValue: usersServiceSpyObj },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    let foundUsers = Array();
    foundUsers.push(new MinimalUser(42, "answer"));
    foundUsers.push(new MinimalUser(2, "first prime"));
    usersServiceSpyObj.findUsers.and.returnValue(of(foundUsers));
    usersServiceSpyObj.getUser.and.returnValue(of(
      Builder(DetailedUser)
        .id(666)
        .username("devil")
        .hash("$6$6")
        .enabled(true)
        .name("Voldemort")
        .version(1234)
        .admin(true)
        .build()
    ));
    fixture.detectChanges();
    expect(component.users).toEqual(foundUsers);
    expect(usersServiceSpyObj.findUsers).toHaveBeenCalledWith("", 0);
    expect(component.isFinding).toBeFalsy();
  });

  it('should toggle visibility of search panel', () => {
    expect(component.showSmallSearchPanel).toBeFalsy();
    component.toggleShowSmallSearchPanel();
    expect(component.showSmallSearchPanel).toBeTruthy();
    component.toggleShowSmallSearchPanel();
    expect(component.showSmallSearchPanel).toBeFalsy();
  });

});
