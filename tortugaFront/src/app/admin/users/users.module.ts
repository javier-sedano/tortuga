import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { NGX_TRANSLATE_CONF } from 'src/app/app.module';
import { UsersComponent} from './users.component'
import { UsersRoutingModule } from './users-routing.module';

@NgModule({
  declarations: [
    UsersComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule.forChild(NGX_TRANSLATE_CONF),
    UsersRoutingModule,
    InfiniteScrollModule,
    FontAwesomeModule,
  ]
})
export class UsersModule { }
