import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Subject, of } from 'rxjs';
import { switchMap, map, catchError } from 'rxjs/operators';
import { NGXLogger } from 'ngx-logger';
import { faSearch, faPlus, faSave, faTrash, faSync, faAngleLeft } from '@fortawesome/free-solid-svg-icons';

import { DebouncerProviderService } from 'src/app/struct/debouncer-provider/debouncer-provider.service';
import { delayedFeedbackDo } from 'src/app/struct/utils';
import { UsersService } from './users-service/users.service';
import { UiDetailedUser } from './ui-detailed-user';
import { MinimalUser } from './users-service/minimal-user';
import { ErrorService } from 'src/app/error/error.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
})
export class UsersComponent implements OnInit {

  filter: string = "";
  users: MinimalUser[];
  selectedUser: UiDetailedUser;
  showSmallSearchPanel: boolean = false;
  isFinding: boolean = false;
  isSaving: boolean = false;
  isDeleting: boolean = false;
  isAdding: boolean = false;
  isGetting: boolean = false;
  faSearch = faSearch;
  faPlus = faPlus;
  faSave = faSave;
  faTrash = faTrash;
  faSync = faSync;
  faAngleLeft = faAngleLeft;

  private debouncerSubject: Subject<string> = new Subject<string>();

  constructor(
    private logger: NGXLogger,
    private activatedRoute: ActivatedRoute,
    private usersService: UsersService,
    private errorService: ErrorService,
    private debouncerProvider: DebouncerProviderService,
    private router: Router,
  ) { }

  private loadInformedItem() {
    let selectingDelayedFeedbackDoer: any;
    this.activatedRoute.paramMap.pipe(
      switchMap(
        (params: ParamMap) => {
          this.isGetting = true;
          selectingDelayedFeedbackDoer = delayedFeedbackDo(() => { this.selectedUser = undefined; });
          let id: string = params.get('id');
          if (id == "") {
            return of(undefined);
          }
          return this.usersService.getUser(Number(params.get('id')))
            .pipe(catchError(error => {
              this.errorService.handleError(error);
              return of(undefined);
            }));
        }
      ),
      map(detailedUser => { return detailedUser ? UiDetailedUser.fromDetailedUser(detailedUser) : undefined })
    ).subscribe( // NOSONAR
      uiDetailedUser => {
        this.isGetting = false;
        selectingDelayedFeedbackDoer.undo(() => {
          // Intentionally blanck, nothing to undo
        });
        this.selectedUser = uiDetailedUser;
        this.showSmallSearchPanel = false;
      }
    );
  }

  private buildDebouncer() {
    this.debouncerProvider.buildDebouncer(
      this.debouncerSubject,
      "",
      (debouncedFilter: string) => {
        this.logger.trace("debounced filter:", debouncedFilter);
        this.isFinding = true;
        return this.usersService.findUsers(debouncedFilter, 0);
      }
    ).subscribe( // NOSONAR
      users => {
        this.isFinding = false;
        document.querySelector("#users-search-results").scrollTop = 0;
        this.users = users;
      },
      error => {
        this.handleFindError(error);
      }
    );
  }

  private handleFindError(error) {
    this.isFinding = false;
    this.users = [];
    this.errorService.handleError(error);
  }

  ngOnInit() {
    this.loadInformedItem();
    this.buildDebouncer();
  }

  debounceFilter() {
    this.logger.trace("debouncing filter:", this.filter);
    this.isFinding = true;
    this.debouncerSubject.next(this.filter);
  }

  add() {
    this.isAdding = true;
    this.usersService.createUser().subscribe( // NOSONAR
      userId => {
        this.isAdding = false;
        this.debounceFilter();
        this.router.navigate(["../" + userId], { relativeTo: this.activatedRoute });
      },
      error => {
        this.isAdding = false;
        this.errorService.handleError(error);
      }
    );
  }

  toggleShowSmallSearchPanel() {
    this.showSmallSearchPanel = !this.showSmallSearchPanel;
  }

  findMore() {
    if (this.isFinding) {
      this.logger.trace("Was already finding, skipping");
      return;
    }
    this.isFinding = true;
    this.usersService.findUsers(this.filter, this.users.length).subscribe( // NOSONAR
      users => {
        this.users = this.users.concat(users);
        this.isFinding = false;
      },
      error => {
        this.handleFindError(error);
      }
    );
  }

  save() {
    this.isSaving = true;
    this.usersService.updateUser(this.selectedUser.toDetailedUser()).subscribe( // NOSONAR
      savedUser => {
        this.selectedUser = UiDetailedUser.fromDetailedUser(savedUser);
        this.isSaving = false;
        this.debounceFilter();
      },
      error => {
        this.isSaving = false;
        this.errorService.handleError(error);
      }
    );
  }

  delete() {
    this.isDeleting = true;
    this.usersService.deleteUser(this.selectedUser.id).subscribe( // NOSONAR
      () => {
        this.router.navigate(['../'], { relativeTo: this.activatedRoute });
        this.selectedUser = undefined;
        this.isDeleting = false;
        this.debounceFilter();
      },
      error => {
        this.isDeleting = false;
        this.errorService.handleError(error);
      }
    );
  }

}
