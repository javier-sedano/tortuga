import { Builder } from 'builder-pattern';
import { DetailedUser } from './users-service/detailed-user';
import { UiDetailedUser } from './ui-detailed-user';

describe('UiDetailedUser', () => {
  it('should create an instance', () => {
    expect(
      Builder(UiDetailedUser)
        .id(1)
        .username("1")
        .hash("$1$1")
        .hashConfirmation("$1$1")
        .enabled(true)
        .name("1 1 1")
        .version(1)
        .admin(true)
        .build()
    ).toBeTruthy();
  });

  it('should create an instance from a DetailedUser', () => {
    let detailedUser: DetailedUser = Builder(DetailedUser)
      .id(1)
      .username("1")
      .hash("$1$1")
      .enabled(true)
      .name("1 1 1")
      .version(1)
      .admin(true)
      .build();
    let expectedUiDetailedUser: UiDetailedUser = Builder(UiDetailedUser)
      .id(1)
      .username("1")
      .hash("$1$1")
      .hashConfirmation("$1$1")
      .enabled(true)
      .name("1 1 1")
      .version(1)
      .admin(true)
      .build();
    let convertedUiDetailedUser: UiDetailedUser = UiDetailedUser.fromDetailedUser(detailedUser);
    expect(convertedUiDetailedUser).toEqual(expectedUiDetailedUser);
  });

  it('should convert to a DetailedUser', () => {
    let expectedDetailedUser: DetailedUser = Builder(DetailedUser)
      .id(1)
      .username("1")
      .hash("$1$1")
      .enabled(true)
      .name("1 1 1")
      .version(1)
      .admin(true)
      .build();
    let uiDetailedUser: UiDetailedUser = Builder(UiDetailedUser)
      .id(1)
      .username("1")
      .hash("$1$1")
      .hashConfirmation("$1$1")
      .enabled(true)
      .name("1 1 1")
      .version(1)
      .admin(true)
      .build();
    let convertedDetailedUser: DetailedUser = uiDetailedUser.toDetailedUser();
    expect(convertedDetailedUser).toEqual(expectedDetailedUser);
  });

  it('should not convert an incorrect state to a DetailedUser', () => {
    try {
      let uiDetailedUser: UiDetailedUser = Builder(UiDetailedUser)
        .id(1)
        .username("1")
        .hash("$1$1")
        .hashConfirmation("$wrong$1")
        .enabled(true)
        .name("1 1 1")
        .version(1)
        .admin(true)
        .build();
      let convertedDetailedUser: DetailedUser = uiDetailedUser.toDetailedUser();
      fail();
    } catch (e) {
      expect(e.message).toEqual("Passwords do not match");
    }
  });

  it('should detect missmatching passwords', () => {
    let uiDetailedUser: UiDetailedUser = Builder(UiDetailedUser)
      .id(1)
      .username("1")
      .hash("$1$1")
      .hashConfirmation("$1$1")
      .enabled(true)
      .name("1 1 1")
      .version(1)
      .admin(true)
      .build();
    expect(uiDetailedUser.passwordsMatch()).toBeTruthy();
    let uiDetailedUserMissmatching: UiDetailedUser = Builder(UiDetailedUser)
      .id(1)
      .username("1")
      .hash("$1$1")
      .hashConfirmation("wrong")
      .enabled(true)
      .name("1 1 1")
      .version(1)
      .admin(true)
      .build();
    expect(uiDetailedUserMissmatching.passwordsMatch()).toBeFalsy();
  });

});
