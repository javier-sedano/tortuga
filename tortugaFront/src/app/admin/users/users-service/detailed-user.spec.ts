import { Builder } from 'builder-pattern';
import { DetailedUser } from './detailed-user';

describe('DetailedUser', () => {
  it('should create an instance', () => {
    expect(
      Builder(DetailedUser)
        .id(1)
        .username("1")
        .hash("$1$1")
        .enabled(true)
        .name("1 1 1")
        .version(1)
        .admin(true)
        .build()
    ).toBeTruthy();
  });
});
