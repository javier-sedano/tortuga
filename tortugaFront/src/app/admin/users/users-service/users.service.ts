import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { NGXLogger } from 'ngx-logger';

import { BASE_REST_URL } from 'src/app/app.constants';import { MinimalUser } from './minimal-user';
import { DetailedUser } from './detailed-user';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private static readonly USERS_URL = BASE_REST_URL + "users";
  private static readonly USERS_FILTER_URL = UsersService.USERS_URL;
  private static readonly USERS_USER_URL = UsersService.USERS_URL + "/";

  constructor(private logger: NGXLogger, private http: HttpClient) { }

  findUsers(filter: string, from: number): Observable<MinimalUser[]> {
    return this.http.get<MinimalUser[]>(UsersService.USERS_FILTER_URL, { params: { filter: filter, from: from.toString() } });
  }

  getUser(id: number): Observable<DetailedUser> {
    return this.http.get<DetailedUser>(UsersService.USERS_USER_URL + id);
  }

  updateUser(user: DetailedUser): Observable<DetailedUser> {
    return this.http.put<DetailedUser>(UsersService.USERS_URL, user);
  }

  deleteUser(id: number): Observable<any> {
    return this.http.delete<string>(UsersService.USERS_USER_URL + id);
  }

  createUser(): Observable<number> {
    return this.http.post<number>(UsersService.USERS_URL, null);
  }

}
