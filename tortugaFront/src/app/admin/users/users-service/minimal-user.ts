export class MinimalUser {
    constructor(
        public id: number,
        public username: string,
    ) { }
}
