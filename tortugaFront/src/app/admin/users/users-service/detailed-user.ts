export class DetailedUser {
    public id: number;
    public username: string;
    public hash: string;
    public enabled: boolean;
    public name: string;
    public version: number;
    public admin: boolean;
}
