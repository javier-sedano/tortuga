import { Component } from '@angular/core';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { SmallMenuToggler } from 'src/app/struct/small-menu-toggler/small-menu-toggler';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
})
export class AdminComponent extends SmallMenuToggler {
  faBars = faBars;

  constructor() { super(); }

}
