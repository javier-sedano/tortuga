import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DummyComponent } from './dummy.component';

@NgModule({
    declarations: [
        DummyComponent,
    ],
})
export class DummyModule { }
