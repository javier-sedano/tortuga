import { TranslateLoader } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';

const NGX_TESTING_TRANSLATIONS: any = {
    "app": {
        "logout": "L0g0ut {{username}}"
    }
};

class FakeLoader implements TranslateLoader {
    getTranslation(lang: string): Observable<any> {
        return of(NGX_TESTING_TRANSLATIONS);
    }
}

export const NGX_TRANSLATE_TESTING_CONF = {
    loader: { provide: TranslateLoader, useClass: FakeLoader },
};
