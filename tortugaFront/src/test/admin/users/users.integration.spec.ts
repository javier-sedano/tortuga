import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute, Router } from '@angular/router';

import { BASE_REST_URL, ERROR_HTTP_RESPONSE } from 'src/testing/testing.constants';
import { ActivatedRouteStub } from 'src/testing/activated-route-stub';
import { AppModule } from 'src/app/app.module';
import { UsersModule } from 'src/app/admin/users/users.module';
import { UsersComponent } from 'src/app/admin/users/users.component';
import { ErrorService } from 'src/app/error/error.service';

describe('Users', () => {
  let component: UsersComponent;
  let fixture: ComponentFixture<UsersComponent>;
  let errorService: ErrorService;

  const USERS_URL: string = BASE_REST_URL + 'users';

  let httpTestingController: HttpTestingController;
  let activatedRouteStub: ActivatedRouteStub;
  let routerSpyObj;

  beforeEach(waitForAsync(() => {
    activatedRouteStub = new ActivatedRouteStub({ id: "" });
    TestBed.configureTestingModule({
      declarations: [
      ],
      imports: [
        AppModule,
        UsersModule,
        HttpClientTestingModule,
      ],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
      ],
    }).compileComponents();
    httpTestingController = TestBed.get(HttpTestingController);
    routerSpyObj = TestBed.get(Router);
    errorService = TestBed.get(ErrorService);
    spyOn(routerSpyObj, "navigate");
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersComponent);
    component = fixture.componentInstance;
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  function testListUsersWithoutFilter() {
    fixture.detectChanges();
    expect(component.isFinding).toEqual(true);

    let foundUsers = Array();
    foundUsers.push({ id: 12, username: "palpatine" });
    foundUsers.push({ id: 34, username: "vader" });
    const req = httpTestingController.expectOne(USERS_URL + "?filter=" + "" + "&from=" + 0);
    expect(req.request.method).toEqual('GET');
    req.flush(foundUsers);

    expect(component.isFinding).toEqual(false);
    expect(component.users).toEqual(foundUsers);
    return foundUsers;
  }

  it('should create with the users from the server', () => {
    testListUsersWithoutFilter();
  });

  it('should create with the users from the server... and detect error', () => {
    fixture.detectChanges();
    expect(component.isFinding).toEqual(true);

    const req = httpTestingController.expectOne(USERS_URL + "?filter=" + "" + "&from=" + 0);
    expect(req.request.method).toEqual('GET');
    req.flush("", ERROR_HTTP_RESPONSE);

    expect(component.isFinding).toBeFalsy();
    expect(component.users).toEqual([]);
    expect(errorService.getErrorMessage()).not.toBe(undefined);
  });

  it('should request more users when requested', () => {
    let foundUsers = testListUsersWithoutFilter();

    expect(component.isFinding).toEqual(false);
    component.findMore();
    expect(component.isFinding).toEqual(true);

    let foundUsers2 = Array();
    foundUsers2.push({ id: 56, username: "leia" });
    foundUsers2.push({ id: 78, username: "luke" });
    const req2 = httpTestingController.expectOne(USERS_URL + "?filter=" + "" + "&from=" + foundUsers.length);
    expect(req2.request.method).toEqual('GET');
    req2.flush(foundUsers2);

    expect(component.isFinding).toEqual(false);
    expect(component.users).toEqual(foundUsers.concat(foundUsers2));
  });

  it('should request more users when requested... and detect error ', () => {
    let foundUsers = testListUsersWithoutFilter();

    expect(component.isFinding).toEqual(false);
    component.findMore();
    expect(component.isFinding).toEqual(true);

    const req2 = httpTestingController.expectOne(USERS_URL + "?filter=" + "" + "&from=" + foundUsers.length);
    expect(req2.request.method).toEqual('GET');
    req2.flush("", ERROR_HTTP_RESPONSE);

    expect(component.isFinding).toEqual(false);
    expect(component.users).toEqual([]);
    expect(errorService.getErrorMessage()).not.toBe(undefined);
  });

  it('should request filtered users when requested', () => {
    testListUsersWithoutFilter();

    component.filter = "qwe";
    expect(component.isFinding).toEqual(false);
    component.debounceFilter();
    expect(component.isFinding).toEqual(true);

    let foundUsers = Array();
    foundUsers.push({ id: 23, username: "solo" });
    foundUsers.push({ id: 45, username: "chwee" });
    const req2 = httpTestingController.expectOne(USERS_URL + "?filter=" + "qwe" + "&from=" + 0);
    expect(req2.request.method).toEqual('GET');
    req2.flush(foundUsers);

    expect(component.isFinding).toEqual(false);
    expect(component.users).toEqual(foundUsers);
  });

  it('should not request more users when there is a pending request', () => {
    let foundUsers = testListUsersWithoutFilter();
    component.isFinding = true;
    component.findMore();
    expect(component.users).toEqual(foundUsers);
    expect(component.isFinding).toEqual(true);
    component.isFinding = false;
  });

  it('should add a user when requested', () => {
    let foundUsers = testListUsersWithoutFilter();
    expect(component.isAdding).toEqual(false);
    component.add();
    expect(component.isAdding).toEqual(true);

    let createdUserId = 33;
    const createRequest = httpTestingController.expectOne(USERS_URL);
    expect(createRequest.request.method).toEqual('POST');
    createRequest.flush(createdUserId);

    expect(component.isAdding).toEqual(false);
    expect(routerSpyObj.navigate).toHaveBeenCalled();
    expect(component.isFinding).toEqual(true);

    fixture.detectChanges();

    foundUsers.push({ id: 33, username: "0_rand" });
    const refreshRequest = httpTestingController.expectOne(USERS_URL + "?filter=" + "" + "&from=" + 0);
    expect(refreshRequest.request.method).toEqual('GET');
    refreshRequest.flush(foundUsers);

    expect(component.isFinding).toEqual(false);
    expect(component.users).toEqual(foundUsers);
  });

  it('should add a user when requested... and detect error', () => {
    testListUsersWithoutFilter();
    expect(component.isAdding).toEqual(false);
    component.add();
    expect(component.isAdding).toEqual(true);

    const createRequest = httpTestingController.expectOne(USERS_URL);
    expect(createRequest.request.method).toEqual('POST');
    createRequest.flush("", ERROR_HTTP_RESPONSE);

    expect(component.isAdding).toEqual(false);
    expect(routerSpyObj.navigate).not.toHaveBeenCalled();
    expect(errorService.getErrorMessage()).not.toBe(undefined);
  });

  function testShowOneUserDetail() {
    expect(component.isGetting).toEqual(false);

    let user78 = { id: 78, username: "luke", hash: "$123$123", enabled: false, name: "Luke Skywalker", version: 1, admin: false };
    let uiUser78 = { id: 78, username: "luke", hash: "$123$123", hashConfirmation: "$123$123", enabled: false, name: "Luke Skywalker", version: 1, admin: false };
    activatedRouteStub.setParamMap({ id: "" + user78.id });

    fixture.detectChanges();
    expect(component.isGetting).toEqual(true);
    expect(component.selectedUser).toEqual(undefined);

    const getRequest = httpTestingController.expectOne(USERS_URL + "/" + user78.id);
    expect(getRequest.request.method).toEqual('GET');
    getRequest.flush(user78);

    expect(component.isGetting).toEqual(false);
    expect(component.selectedUser).toEqual(jasmine.objectContaining(uiUser78));
  }

  function testShowOneUser() {
    testListUsersWithoutFilter();
    testShowOneUserDetail();
  }

  it('should show one user ', () => {
    testShowOneUser();
  });

  it('should show one user... and detect error', () => {
    expect(component.isGetting).toEqual(false);
    const USER_ID = 666;
    activatedRouteStub.setParamMap({ id: "" + USER_ID });

    testListUsersWithoutFilter();

    fixture.detectChanges();
    expect(component.isGetting).toEqual(true);

    const getRequest = httpTestingController.expectOne(USERS_URL + "/" + USER_ID);
    expect(getRequest.request.method).toEqual('GET');
    getRequest.flush("", ERROR_HTTP_RESPONSE);

    expect(component.isGetting).toEqual(false);
    expect(component.selectedUser).toBe(undefined);
    expect(errorService.getErrorMessage()).not.toBe(undefined);
  });

  it('should show one user and then another user', () => {
    testShowOneUser()
    testShowOneUserDetail();
  });

  it('should show small search panel and hide it when showing note', () => {
    expect(component.showSmallSearchPanel).toEqual(false);
    component.toggleShowSmallSearchPanel();
    expect(component.showSmallSearchPanel).toEqual(true);
    testShowOneUser();
    expect(component.showSmallSearchPanel).toEqual(false);
  });

  it('should save a user', () => {
    testShowOneUser();

    component.selectedUser.username = "look";
    component.selectedUser.hash = "newPass";
    component.selectedUser.hashConfirmation = "newPass";
    component.selectedUser.enabled = true;
    component.selectedUser.name = "Look Skywalker"
    component.selectedUser.admin = true;
    let expectedUserToSave = {
      id: component.selectedUser.id,
      username: component.selectedUser.username,
      hash: component.selectedUser.hash,
      enabled: component.selectedUser.enabled,
      name: component.selectedUser.name,
      version: component.selectedUser.version,
      admin: component.selectedUser.admin,
    };

    expect(component.isSaving).toEqual(false);
    component.save();
    expect(component.isSaving).toEqual(true);

    const req = httpTestingController.expectOne(USERS_URL);
    expect(req.request.method).toEqual('PUT');
    expect(req.request.body).toEqual(jasmine.objectContaining(expectedUserToSave));
    let savedUsed = {
      id: expectedUserToSave.id,
      username: expectedUserToSave.username,
      hash: expectedUserToSave.hash,
      hashConfirmation: expectedUserToSave.hash,
      enabled: expectedUserToSave.enabled,
      name: expectedUserToSave.name,
      version: (expectedUserToSave.version + 1),
      admin: expectedUserToSave.admin,
    };
    req.flush(savedUsed);

    expect(component.isSaving).toEqual(false);
    testListUsersWithoutFilter();
    expect(component.selectedUser).toEqual(jasmine.objectContaining(savedUsed));
  });

  it('should save a user... and detect error', () => {
    testShowOneUser();
    let shownUser = component.selectedUser;

    expect(component.isSaving).toEqual(false);
    component.save();
    expect(component.isSaving).toEqual(true);

    const req = httpTestingController.expectOne(USERS_URL);
    expect(req.request.method).toEqual('PUT');
    req.flush("", ERROR_HTTP_RESPONSE);

    expect(component.isSaving).toEqual(false);
    expect(component.selectedUser).toEqual(shownUser);
    expect(errorService.getErrorMessage()).not.toBe(undefined);
  });

  it('should not save a user with different passwords', () => {
    testShowOneUser();

    component.selectedUser.username = "look";
    component.selectedUser.hash = "newPass";
    component.selectedUser.hashConfirmation = "newP";
    component.selectedUser.enabled = true;
    component.selectedUser.name = "Look Skywalker"
    component.selectedUser.admin = true;
    try {
      component.save();
      fail();
    } catch (e) {
      expect(e.message).toEqual("Passwords do not match");
    }
  });

  it('should delete a user and refresh', () => {
    testShowOneUser();
    expect(component.isDeleting).toEqual(false);
    component.delete();
    expect(component.isDeleting).toEqual(true);

    const req = httpTestingController.expectOne(USERS_URL + "/" + component.selectedUser.id);
    expect(req.request.method).toEqual('DELETE');
    req.flush("");

    expect(component.isDeleting).toEqual(false);
    testListUsersWithoutFilter();
  });

  it('should delete a user and refresh... and detect error', () => {
    testShowOneUser();
    let shownUser = component.selectedUser;
    expect(component.isDeleting).toEqual(false);
    component.delete();
    expect(component.isDeleting).toEqual(true);

    const req = httpTestingController.expectOne(USERS_URL + "/" + component.selectedUser.id);
    expect(req.request.method).toEqual('DELETE');
    req.flush("", ERROR_HTTP_RESPONSE);

    expect(component.isDeleting).toEqual(false);
    expect(component.selectedUser).toEqual(shownUser);
    expect(errorService.getErrorMessage()).not.toBe(undefined);
  });

});
