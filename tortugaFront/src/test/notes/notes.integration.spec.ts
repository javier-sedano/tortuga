import { ComponentFixture, TestBed, fakeAsync, waitForAsync } from '@angular/core/testing';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute, Router } from '@angular/router';

import { BASE_REST_URL, ERROR_HTTP_RESPONSE } from 'src/testing/testing.constants';
import { ActivatedRouteStub } from 'src/testing/activated-route-stub';
import { AppModule } from 'src/app/app.module';
import { NotesModule } from 'src/app/notes/notes.module';
import { NotesComponent } from 'src/app/notes/notes.component';
import { ErrorService } from 'src/app/error/error.service';

describe('Notes', () => {
  let component: NotesComponent;
  let fixture: ComponentFixture<NotesComponent>;
  let errorService: ErrorService;

  const NOTES_URL: string = BASE_REST_URL + 'notes';

  let httpTestingController: HttpTestingController;
  let activatedRouteStub: ActivatedRouteStub;
  let routerSpyObj;

  beforeEach(waitForAsync(() => {
    activatedRouteStub = new ActivatedRouteStub({ id: "" });
    TestBed.configureTestingModule({
      declarations: [
      ],
      imports: [
        AppModule,
        NotesModule,
        HttpClientTestingModule,
      ],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
      ],
    }).compileComponents();
    httpTestingController = TestBed.get(HttpTestingController);
    routerSpyObj = TestBed.get(Router);
    errorService = TestBed.get(ErrorService);
    spyOn(routerSpyObj, "navigate");
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotesComponent);
    component = fixture.componentInstance;
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  function testListNotesWithoutFilter() {
    fixture.detectChanges();
    expect(component.isFinding).toEqual(true);

    let foundNotes = Array();
    foundNotes.push({ id: 42, title: "answer" });
    foundNotes.push({ id: 2, title: "first prime" });
    const req = httpTestingController.expectOne(NOTES_URL + "?filter=" + "" + "&from=" + 0);
    expect(req.request.method).toEqual('GET');
    req.flush(foundNotes);

    expect(component.isFinding).toEqual(false);
    expect(component.notes).toEqual(foundNotes);
    return foundNotes;
  }

  it('should create with the notes from the server', () => {
    testListNotesWithoutFilter();
  });

  it('should create with the notes from the server... and detect error ', () => {
    fixture.detectChanges();
    expect(component.isFinding).toEqual(true);

    const req = httpTestingController.expectOne(NOTES_URL + "?filter=" + "" + "&from=" + 0);
    expect(req.request.method).toEqual('GET');
    req.flush("", ERROR_HTTP_RESPONSE);

    expect(component.isFinding).toEqual(false);
    expect(component.notes).toEqual([]);
    expect(errorService.getErrorMessage()).not.toBe(undefined);
  });

  it('should request more notes when requested', () => {
    let foundNotes = testListNotesWithoutFilter();

    expect(component.isFinding).toEqual(false);
    component.findMore();
    expect(component.isFinding).toEqual(true);

    let foundNotes2 = Array();
    foundNotes2.push({ id: 43, title: "answer II" });
    foundNotes2.push({ id: 3, title: "first prime II" });
    const req2 = httpTestingController.expectOne(NOTES_URL + "?filter=" + "" + "&from=" + foundNotes.length);
    expect(req2.request.method).toEqual('GET');
    req2.flush(foundNotes2);

    expect(component.isFinding).toEqual(false);
    expect(component.notes).toEqual(foundNotes.concat(foundNotes2));
  });

  it('should request more notes when requested... and detect error', () => {
    let foundNotes = testListNotesWithoutFilter();

    expect(component.isFinding).toEqual(false);
    component.findMore();
    expect(component.isFinding).toEqual(true);

    const req2 = httpTestingController.expectOne(NOTES_URL + "?filter=" + "" + "&from=" + foundNotes.length);
    expect(req2.request.method).toEqual('GET');
    req2.flush("", ERROR_HTTP_RESPONSE);

    expect(component.isFinding).toEqual(false);
    expect(component.notes).toEqual([]);
    expect(errorService.getErrorMessage()).not.toBe(undefined);
  });

  it('should request filtered notes when requested', () => {
    testListNotesWithoutFilter();

    component.filter = "qwe";
    expect(component.isFinding).toEqual(false);
    component.debounceFilter();
    expect(component.isFinding).toEqual(true);

    let foundNotes = Array();
    foundNotes.push({ id: 44, title: "answer III" });
    foundNotes.push({ id: 4, title: "first prime III" });
    const req2 = httpTestingController.expectOne(NOTES_URL + "?filter=" + "qwe" + "&from=" + 0);
    expect(req2.request.method).toEqual('GET');
    req2.flush(foundNotes);

    expect(component.isFinding).toEqual(false);
    expect(component.notes).toEqual(foundNotes);
  });

  it('should not request more notes when there is a pending request', () => {
    let foundNotes = testListNotesWithoutFilter();
    component.isFinding = true;
    component.findMore();
    expect(component.notes).toEqual(foundNotes);
    expect(component.isFinding).toEqual(true);
    component.isFinding = false;
  });

  it('should add a note when requested', () => {
    let foundNotes = testListNotesWithoutFilter();
    expect(component.isAdding).toEqual(false);
    component.add();
    expect(component.isAdding).toEqual(true);

    let createdNoteId = 3;
    const createRequest = httpTestingController.expectOne(NOTES_URL);
    expect(createRequest.request.method).toEqual('POST');
    createRequest.flush(createdNoteId);

    expect(component.isAdding).toEqual(false);
    expect(routerSpyObj.navigate).toHaveBeenCalled();
    expect(component.isFinding).toEqual(true);

    fixture.detectChanges();

    foundNotes.push({ id: createdNoteId, title: "new" });
    const refreshRequest = httpTestingController.expectOne(NOTES_URL + "?filter=" + "" + "&from=" + 0);
    expect(refreshRequest.request.method).toEqual('GET');
    refreshRequest.flush(foundNotes);

    expect(component.isFinding).toEqual(false);
    expect(component.notes).toEqual(foundNotes);
  });

  it('should add a note when requested... and detect error', () => {
    testListNotesWithoutFilter();
    expect(component.isAdding).toEqual(false);
    component.add();
    expect(component.isAdding).toEqual(true);

    const createRequest = httpTestingController.expectOne(NOTES_URL);
    expect(createRequest.request.method).toEqual('POST');
    createRequest.flush("", ERROR_HTTP_RESPONSE);

    expect(component.isAdding).toEqual(false);
    expect(routerSpyObj.navigate).not.toHaveBeenCalled();
    expect(errorService.getErrorMessage()).not.toBe(undefined);
  });

  function testShowOneNoteDetail() {
    expect(component.isGetting).toEqual(false);
 
    let note42 = { id: 42, title: "answerX", content: "to life", version: 1 };
    activatedRouteStub.setParamMap({ id: "" + note42.id });

    fixture.detectChanges();
    expect(component.isGetting).toEqual(true);
    expect(component.selectedNote).toEqual(undefined);

    const getRequest = httpTestingController.expectOne(NOTES_URL + "/" + note42.id);
    expect(getRequest.request.method).toEqual('GET');
    getRequest.flush(note42);

    expect(component.isGetting).toEqual(false);
    expect(component.selectedNote).toEqual(note42);
  }

  function testShowOneNote() {
    testListNotesWithoutFilter();
    testShowOneNoteDetail();
  }


  it('should show one note', () => {
    testShowOneNote();
  });

  it('should show one note... and detect error', () => {
    expect(component.isGetting).toEqual(false);
    const NOTE_ID = 666;
    activatedRouteStub.setParamMap({ id: "" + NOTE_ID });

    testListNotesWithoutFilter();

    fixture.detectChanges();
    expect(component.isGetting).toEqual(true);

    const getRequest = httpTestingController.expectOne(NOTES_URL + "/" + NOTE_ID);
    expect(getRequest.request.method).toEqual('GET');
    getRequest.flush("", ERROR_HTTP_RESPONSE);

    expect(component.isGetting).toEqual(false);
    expect(component.selectedNote).toBe(undefined);
    expect(errorService.getErrorMessage()).not.toBe(undefined);
  });

  it('should show one note and then another note', () => {
    testShowOneNote();
    testShowOneNoteDetail();
  });

  it('should show small search panel and hide it when showing note', () => {
    expect(component.showSmallSearchPanel).toEqual(false);
    component.toggleShowSmallSearchPanel();
    expect(component.showSmallSearchPanel).toEqual(true);
    testShowOneNote();
    expect(component.showSmallSearchPanel).toEqual(false);
  });

  it('should save a note', () => {
    testShowOneNote();

    component.selectedNote.title = "ultimate answer";
    component.selectedNote.content = "to life universe and everything";
    let expectedNoteToSave = {
      id: component.selectedNote.id,
      title: component.selectedNote.title,
      content: component.selectedNote.content,
      version: component.selectedNote.version,
    };

    expect(component.isSaving).toEqual(false);
    component.save();
    expect(component.isSaving).toEqual(true);

    const req = httpTestingController.expectOne(NOTES_URL);
    expect(req.request.method).toEqual('PUT');
    expect(req.request.body).toEqual(expectedNoteToSave);
    let savedNote = {
      id: expectedNoteToSave.id,
      title: expectedNoteToSave.title,
      content: expectedNoteToSave.content,
      version: (expectedNoteToSave.version + 1),
    };
    req.flush(savedNote);

    expect(component.isSaving).toEqual(false);
    testListNotesWithoutFilter();
    expect(component.selectedNote).toEqual(savedNote);
  });

  it('should save a note... and detect error', () => {
    testShowOneNote();
    let shownNote = component.selectedNote;

    expect(component.isSaving).toEqual(false);
    component.save();
    expect(component.isSaving).toEqual(true);

    const req = httpTestingController.expectOne(NOTES_URL);
    expect(req.request.method).toEqual('PUT');
    req.flush("", ERROR_HTTP_RESPONSE);

    expect(component.isSaving).toEqual(false);
    expect(component.selectedNote).toEqual(shownNote);
    expect(errorService.getErrorMessage()).not.toBe(undefined);
  });

  it('should delete a note and refresh', () => {
    testShowOneNote();
    expect(component.isDeleting).toEqual(false);
    component.delete();
    expect(component.isDeleting).toEqual(true);

    const req = httpTestingController.expectOne(NOTES_URL + "/" + component.selectedNote.id);
    expect(req.request.method).toEqual('DELETE');
    req.flush("");

    expect(component.isDeleting).toEqual(false);
    testListNotesWithoutFilter();
  });

  it('should delete a note and refresh... and detect error', () => {
    testShowOneNote();
    let shownNote = component.selectedNote;
    expect(component.isDeleting).toEqual(false);
    component.delete();
    expect(component.isDeleting).toEqual(true);

    const req = httpTestingController.expectOne(NOTES_URL + "/" + component.selectedNote.id);
    expect(req.request.method).toEqual('DELETE');
    req.flush("", ERROR_HTTP_RESPONSE);

    expect(component.isDeleting).toEqual(false);
    expect(component.selectedNote).toEqual(shownNote);
    expect(errorService.getErrorMessage()).not.toBe(undefined);
  });

});
