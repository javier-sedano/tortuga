import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { AppModule } from 'src/app/app.module';
import { ErrorComponent } from 'src/app/error/error.component';
import { ErrorService } from 'src/app/error/error.service';

describe('Error', () => {
  const DEFAULT_I18N: string = "error.i18nCode.default";
  const ERROR_DIV: string = "#app-error-dialog";

  let component: ErrorComponent;
  let fixture: ComponentFixture<ErrorComponent>;
  let errorService: ErrorService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
      ],
      imports: [
        AppModule,
      ],
    }).compileComponents();
    errorService = TestBed.get(ErrorService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorComponent);
    component = fixture.componentInstance;
  });

  function testIsHidden() {
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector(ERROR_DIV)).toBeNull();
  }

  function testIsShown() {
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector(ERROR_DIV)).not.toBeNull();
    expect(component.getErrorMessage()).toEqual(DEFAULT_I18N);
  }

  function testDismiss() {
    component.dismissError();
    testIsHidden();
  }

  it('should start hidden', () => {
    testIsHidden();
  });

  it('should show on error and hide on dismiss', () => {
    testIsHidden();
    errorService.handleError(new Error());
    testIsShown();
    testDismiss();
  });

});
