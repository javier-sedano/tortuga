Sample web application based on Spring Boot and Angular.

The architecture of this project is an obvious overkill for an application this small (a couple of windows and a couple of tables), but it is an architectural sample for larger applications.

Master branch:
[![pipeline status](https://gitlab.com/javier-sedano/tortuga/badges/master/pipeline.svg)](https://gitlab.com/javier-sedano/tortuga/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.tortuga&metric=alert_status)](https://sonarcloud.io/dashboard?id=com.odroid.tortuga)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.tortuga&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=com.odroid.tortuga)
<details>
<summary>Quality details</summary>

[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.tortuga&metric=bugs)](https://sonarcloud.io/dashboard?id=com.odroid.tortuga)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.tortuga&metric=code_smells)](https://sonarcloud.io/dashboard?id=com.odroid.tortuga)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.tortuga&metric=coverage)](https://sonarcloud.io/dashboard?id=com.odroid.tortuga)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.tortuga&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=com.odroid.tortuga)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.tortuga&metric=ncloc)](https://sonarcloud.io/dashboard?id=com.odroid.tortuga)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.tortuga&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=com.odroid.tortuga)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.tortuga&metric=security_rating)](https://sonarcloud.io/dashboard?id=com.odroid.tortuga)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.tortuga&metric=sqale_index)](https://sonarcloud.io/dashboard?id=com.odroid.tortuga)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.tortuga&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=com.odroid.tortuga)

[Sonar analysis](https://sonarcloud.io/dashboard?id=com.odroid.tortuga)
</details>

Keywords: spring, spring-boot, spring-security, blowfish, spring-data, jpa, buildinfo, gradle, lombok, retrofit, retrofit2, junit, mockmvc, mockito, Wiremock, git, sonar, sonarcloud, jacoco, flyway, hsqldb, angular, jasmine, karma, istanbul, docker, continuous integration, continuous quality, continuous deployment, docker, kubernetes, GKE, GCE, gitlab ci, i18n, themes, w3css, responsive, html5, mobile first, infinite-scroll, openjdk, openjdk11, AdoptOpenJDK, EditorConfig

(C) Javier Sedano 2013-2021.

Instructions:

  * Clone repo
    * If dit download it, GIT will not be available, so `com.gorylenko.gradle-git-properties` must be disabled in `build.gradle`.
  * Requirements:
    * Java 11 compiler. OpenJDK11 from AdoptOpenJDK with openJ9 (see link below) is used in production, so it is suggested for building and testing.
    * NodeJS version 12 or higher with npm.
  * To quickly run it: `gradlew clean bootRun` and browse [tortuga](http://localhost:8080/tortuga) or `gradlew applicationBrowse`.
    * Administrator: `a`, password `a`.
    * Plain user: `b`, password `b`
    * This generates DB on startup and adds some sample data.
  * To generate deliverables (remember to create database and edit application.properties if needed):
    * To generate an autorunnable jar: `gradlew bootJar`.
    * To generate a deployable war: `gradlew bootWar`.
    * To create a docker image:
      * `gradlew bootJar`.
      * VER=`cat gradle.properties | grep project_version | tr -d '\n' | tr -d '\r' | cut -d= -f2`
      * TAG="${VER}-$CI_COMMIT_SHORT_SHA"
      * `cat deploy/app/docker/Dockerfile | sed s/VERSION/$TAG/g | docker build -t tortuga:$TAG -f - .`
        * Export docker image to file: `docker save tortuga | gzip > ../tortuga-docker.tgz`.
        * Or push it to a registry (this process is done during CI/CD and the latests iamge is pushed to [GitLab registry](https://gitlab.com/javier-sedano/tortuga/container_registry))
      * Database is migrated to latest version upon startup using flyway, but sample data is not added by default. Add `-Dspring.flyway.locations=classpath:/db/migration,classpath:/db/sample` to add sample data, if desired
  * To study or develop it:
    * Windows is assumed in many dev tasks (not so for CI tasks); either use Windows or port them to Unix (should be pretty straightforward: look for `cmd /c`).
    * Backend (instructions for IntelliJ 2019.2, but obviously other IDEs are usable):
      * Import as gradle project into IntelliJ:
        * Suggested: enable auto-import, build and run / run tests using IntelliJ.
      * Lombok plugin is strongly suggested:
        * Install plugin.
        * Enable Annotation Processing: Settings / Build, Execution, Deployment / Compiler / Annotation Processors / Enable.
        * Let IntelliJ upgrade the plugin and restart.
      * Configure IntelliJ to "Build and run" and "Run tests" using IntelliJ (default is gradle). Do it under Settings / Build, Execution, Deployment / Build Tools / Gradle / Gradle (or use the wrench button in the gradle panel).
      * A launcher for IntelliJ is included (Tortuga w/Sample): it launches backend and watch-builds frontend. Otherwise
          * Launch in IDE with `-Dspring.config.location=src/test/resources/test.properties -Dspring.datasource.url=jdbc:hsqldb:file:build/db/app-hsql-with-sample-data/app.hsql;shutdown=true`.
            * To emulate the deployed behaviour add `-Xmx70m`.
            * This creates (or migrates) a minimal DB on startup:
              * To create (or migrate) a DB with sample data, add `-Dspring.flyway.locations=classpath:/db/migration,classpath:/db/sample`.
              * DB can aldo be created using `gradle createDB` (for minimal DB) or `createDbWithSampleData` (for demos or exploratory tests).
          * Watch-build frontend with `gradlew ngBuildWatch`
      * Launch tests in IDE or with `gradlew test`.
    * Frontend (instructions for Visual Studio Code, but obviously other IDEs are usable):
      * Import tortugaFront/ directory into VisualStudioCode.
      * Add EditorConfig plugin, so .editorconfig is honored.
      * To launch tests in watch mode: `npm run ngTest`.
      * The app is served through backend (see launcher and gradle watch-build above).
    * Sonarqube is used as analysis tool, but modern Sonar does not allow unauthenticated analysis. So launch Sonarqube, browsse http://localhost:9000, login with `admin/admin`, you will be asked to change the password... change to `admin/admin1`.
  * Master branch is continuously deployed to [https://jsedano.duckdns.org/tortuga](https://jsedano.duckdns.org/tortuga). Notice that different passwords are used there, so you may not be able to login. Continuous deployment uses SSH keys and default passwords that are not shown in source. Notice that it is deployed in a severely limited Google Compute Engine virtual machine, so performance may not be good or may not even work.
  * Master branch is continuously deployed to [https://tortuga-jsedano.duckdns.org/tortuga/](https://tortuga-jsedano.duckdns.org/tortuga). Notice that different passwords are used there, so you may not be able to login. Continuous deployment uses SSH keys and default passwords that are not shown in source. Notice that it is deployed to a Google Kubernetes Engine cluster which is a paid-service, so I may decide to stop doing it without further notice.
  * A Windows service is downloadable from [https://jsedano.duckdns.org/tortugaWinUpdate/](https://jsedano.duckdns.org/tortugaWinUpdate/).
  * Hint for derived project:
    * Replace `tortuga` to `xxx`
    * Replace `Tortuga` to `Xxx`
    * Rename `tortugaFront` to `xxxFront` or better yet `xxx_front` (which complies with package name convention)
    * Rename `src\...\tortuga\...` to `src\...\xxx\...`
    * Adapt (or remove) CD process
  * An Android wrapper is also available: [https://gitlab.com/javier-sedano/tortugaandroid](https://gitlab.com/javier-sedano/tortugaandroid)

Useful links:

* https://angular.io/
* https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/
* https://docs.spring.io/spring/docs/5.1.10.RELEASE/spring-framework-reference/
* https://docs.spring.io/spring-data/jpa/docs/2.0.9.RELEASE/reference/html/
* https://flywaydb.org/
* https://www.baeldung.com/jpa-optimistic-locking
* https://adoptopenjdk.net/
* https://docs.spring.io/spring-security/site/docs/5.0.12.RELEASE/reference/htmlsingle/
* https://docs.spring.io/spring-boot/docs/current/actuator-api/html/
* https://spring.io/guides/tutorials/spring-security-and-angular-js/
* https://github.com/dorongold/gradle-task-tree
* https://github.com/OSSIndex/ossindex-gradle-plugin/
* https://projectlombok.org/features/all
* https://square.github.io/retrofit/
* https://square.github.io/okhttp/
* http://wiremock.org/
* https://docs.npmjs.com/cli-documentation/
* https://www.npmjs.com/package/cross-var
* https://www.npmjs.com/package/rimraf
* https://jasmine.github.io/2.5/introduction
* http://karma-runner.github.io/2.0/config/configuration-file.html
* https://www.w3schools.com/w3css/
* https://sass-lang.com/documentation
* https://fontawesome.com/icons?d=gallery&s=brands,regular,solid&m=free
* https://www.npmjs.com/package/ngx-logger
* https://github.com/ngx-translate/core
* https://github.com/denniske/ngx-translate-multi-http-loader
* https://www.learnrxjs.io/
* https://github.com/orizens/ngx-infinite-scroll
* https://docs.gitlab.com/ee/ci/yaml/
* https://sonarcloud.io/web_api/api/qualitygates
* https://www.2uo.de/myths-about-urandom/
* https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner+for+Gradle
* https://docs.sonarqube.org/latest/analysis/analysis-parameters/
* https://kubernetes.io/docs/tutorials/
* https://cloud.google.com/kubernetes-engine/docs/
* https://kubernetes.io/docs/reference/
* https://github.com/kohsuke/winsw
