package com.odroid.tortuga.test.mock.service.iface;

public interface FailerService {
  void fail();
}
