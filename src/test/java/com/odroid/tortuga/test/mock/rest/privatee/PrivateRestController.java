package com.odroid.tortuga.test.mock.rest.privatee;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

import static com.odroid.tortuga.rest.RestConstants.BASE_REST_URL;

@RestController
@RequestMapping(BASE_REST_URL + "private/")
public class PrivateRestController {
  @GetMapping("me")
  public String whoAmI(Principal principal) {
    return principal.getName();
  }
}
