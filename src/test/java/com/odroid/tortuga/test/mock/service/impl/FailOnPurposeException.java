package com.odroid.tortuga.test.mock.service.impl;

import com.odroid.tortuga.service.iface.errors.AppException;

import java.util.AbstractMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FailOnPurposeException extends AppException {

  private static final String I18N = "error.backend.onPurpose";

  private static final String REASON = "reason";
  private static final String CODE = "code";

  public FailOnPurposeException(String with) {
    super(
            I18N,
            Stream.of(
                    new AbstractMap.SimpleEntry<>(REASON, with),
                    new AbstractMap.SimpleEntry<>(CODE, "qwerty")
            ).collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue))
    );
  }
}
