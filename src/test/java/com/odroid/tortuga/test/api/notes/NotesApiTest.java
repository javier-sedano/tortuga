package com.odroid.tortuga.test.api.notes;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.odroid.tortuga.test.AppTestPropertySource;
import com.odroid.tortuga.SpringBootApp;
import com.odroid.tortuga.service.iface.notes.DetailedNote;
import com.odroid.tortuga.service.iface.notes.MinimalNote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static com.odroid.tortuga.test.api.ApiTestConstants.BASE_REST_URL;
import static com.odroid.tortuga.test.api.ApiTestUtils.testAdmin;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = SpringBootApp.class,
        properties = {
                "logging.level.org.springframework.test.web.servlet.result=DEBUG",
                "app.lorem-base-url=http://localhost:8082/"
        })
@AppTestPropertySource
class NotesApiTest {

  private static final String NOTES_URL = BASE_REST_URL + "notes";
  private static final String NOTES_FILTER_PARAM = "filter";
  private static final String DEFAULT_TITLE = ".";
  private static final String DEFAULT_CONTENT = "";

  @Autowired
  private WebApplicationContext context;

  private MockMvc mockMvc;

  private final ObjectMapper jackson = new ObjectMapper();

  @BeforeEach
  void configureMockMvc() {
    mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
  }

  @Test
  void testCreateRetrieveDelete() throws Exception {
    Long noteId = createNote();
    try {
      DetailedNote gotNote = retrieveNote(noteId);
      System.out.println(gotNote);
      assertEquals(noteId, gotNote.getId());
      assertEquals(DEFAULT_TITLE, gotNote.getTitle());
      assertEquals(DEFAULT_CONTENT, gotNote.getContent());
    } finally {
      deleteNote(noteId);
    }
  }

  @Test
  void testFindWithoutFilter() throws Exception {
    Long noteId = createNote();
    try {

      MvcResult mvcResult = mockMvc.perform(
              get(NOTES_URL).with(testAdmin()))
              .andExpect(status().isOk())
              .andExpect(content().contentType(APPLICATION_JSON_VALUE))
              .andReturn();
      List<MinimalNote> minimalNotes = jackson.readValue(
              mvcResult.getResponse().getContentAsString(),
              new TypeReference<List<MinimalNote>>() {
              });
      assertEquals(1, minimalNotes.size());
    } finally {
      deleteNote(noteId);
    }
  }

  @Test
  void testFindWithFilter() throws Exception {
    Long noteId = createNote();
    try {

      MvcResult mvcResult = mockMvc.perform(
              get(NOTES_URL).param(NOTES_FILTER_PARAM, DEFAULT_TITLE).with(testAdmin()))
              .andExpect(status().isOk())
              .andExpect(content().contentType(APPLICATION_JSON_VALUE))
              .andReturn();
      List<MinimalNote> minimalNotes = jackson.readValue(
              mvcResult.getResponse().getContentAsString(),
              new TypeReference<List<MinimalNote>>() {
              });
      assertEquals(1, minimalNotes.size());
      MvcResult mvcResultEmpty = mockMvc.perform(
              get(NOTES_URL).param(NOTES_FILTER_PARAM, "qwertyuiop").with(testAdmin()))
              .andExpect(status().isOk())
              .andExpect(content().contentType(APPLICATION_JSON_VALUE))
              .andReturn();
      List<MinimalNote> minimalNotesEmpty = jackson.readValue(
              mvcResultEmpty.getResponse().getContentAsString(),
              new TypeReference<List<MinimalNote>>() {
              });
      assertEquals(0, minimalNotesEmpty.size());
    } finally {
      deleteNote(noteId);
    }
  }


  @Test
  void testCreateUpdate() throws Exception {
    Long noteId = createNote();
    try {
      DetailedNote gotNote = retrieveNote(noteId);
      gotNote.setTitle("zxc");
      gotNote.setContent("vbnm");

      MvcResult mvcResult = mockMvc.perform(
              put(NOTES_URL).with(testAdmin()).with(csrf())
                      .contentType(APPLICATION_JSON_VALUE)
                      .content(jackson.writeValueAsString(gotNote)))
              .andExpect(status().isOk())
              .andExpect(content().contentType(APPLICATION_JSON_VALUE))
              .andReturn();
      DetailedNote modifiedNote = jackson.readValue(
              mvcResult.getResponse().getContentAsString(),
              DetailedNote.class);

      assertEquals("zxc", modifiedNote.getTitle());
      assertEquals("vbnm", modifiedNote.getContent());
    } finally {
      deleteNote(noteId);
    }
  }

  private Long createNote() throws Exception {
    MvcResult mvcResult = mockMvc.perform(
            post(NOTES_URL).with(testAdmin()).with(csrf()))
            .andExpect(status().isOk())
            .andReturn();
    return jackson.readValue(
            mvcResult.getResponse().getContentAsString(),
            Long.class);
  }

  private DetailedNote retrieveNote(Long noteId) throws Exception {
    MvcResult mvcResult = mockMvc.perform(
            get(NOTES_URL + "/{id}", noteId).with(testAdmin()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andReturn();
    return jackson.readValue(
            mvcResult.getResponse().getContentAsString(),
            DetailedNote.class);
  }

  private void deleteNote(Long noteId) throws Exception {
    mockMvc.perform(
            delete(NOTES_URL + "/{id}", noteId).with(testAdmin()).with(csrf()))
            .andExpect(status().isOk());
    mockMvc.perform(
            get(NOTES_URL + "/{id}", noteId).with(testAdmin()))
            .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
            .andExpect(content().json("{\"i18nCode\":\"common.errorI18nCode.notFound\",\"args\":{\"id\":\"" + noteId + "\",\"entity\":\"Note\"}}"));
  }

}
