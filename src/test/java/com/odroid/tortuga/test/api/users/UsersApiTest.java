package com.odroid.tortuga.test.api.users;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.odroid.tortuga.test.AppTestPropertySource;
import com.odroid.tortuga.SpringBootApp;
import com.odroid.tortuga.service.iface.users.DetailedUser;
import com.odroid.tortuga.service.iface.users.MinimalUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static com.odroid.tortuga.test.api.ApiTestConstants.BASE_REST_URL;
import static com.odroid.tortuga.test.api.ApiTestUtils.testAdmin;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = SpringBootApp.class,
        properties = {
                "logging.level.org.springframework.test.web.servlet.result=DEBUG"
        })
@AppTestPropertySource
class UsersApiTest {

  private static final String USERS_URL = BASE_REST_URL + "users";
  private static final String USERS_FILTER_PARAM = "filter";

  @Autowired
  private WebApplicationContext context;

  @Autowired
  private NamedParameterJdbcTemplate jdbcTemplate;

  @Autowired
  private PasswordEncoder passwordEncoder;

  private MockMvc mockMvc;

  private final ObjectMapper jackson = new ObjectMapper();

  @BeforeEach
  void configureMockMvc() {
    mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
  }

  @Test
  void testCreateRetrieveDelete() throws Exception {
    Long userId = createUser();
    try {
      DetailedUser gotUser = retrieveUser(userId);
      assertTrue(gotUser.getUsername().startsWith("0_"));
      assertTrue(gotUser.getHash().length() > 0);
      assertTrue(gotUser.getName().length() > 0);
      assertFalse(gotUser.getEnabled());
      assertFalse(gotUser.getAdmin());
    } finally {
      deleteUser(userId);
    }
  }

  @Test
  void testFindWithoutFilter() throws Exception {
    MvcResult mvcResult = mockMvc.perform(
            get(USERS_URL).with(testAdmin()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andReturn();
    List<MinimalUser> minimalUsers = jackson.readValue(
            mvcResult.getResponse().getContentAsString(),
            new TypeReference<List<MinimalUser>>() {
            });
    assertEquals(3, minimalUsers.size());
  }

  @Test
  void testFindWithEmptyFilter() throws Exception {
    MvcResult mvcResult = mockMvc.perform(
            get(USERS_URL).param(USERS_FILTER_PARAM, "").with(testAdmin()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andReturn();
    List<MinimalUser> minimalUsers = jackson.readValue(
            mvcResult.getResponse().getContentAsString(),
            new TypeReference<List<MinimalUser>>() {
            });
    assertEquals(3, minimalUsers.size());
  }

  @Test
  void testFindWithFilter() throws Exception {
    MvcResult mvcResult = mockMvc.perform(
            get(USERS_URL).param(USERS_FILTER_PARAM, "bonifacio").with(testAdmin()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andReturn();
    List<MinimalUser> minimalUsers = jackson.readValue(
            mvcResult.getResponse().getContentAsString(),
            new TypeReference<List<MinimalUser>>() {
            });
    assertEquals(1, minimalUsers.size());
  }

  @Test
  void testCreateUpdate() throws Exception {
    Long userId = createUser();
    try {
      DetailedUser gotUser = retrieveUser(userId);
      gotUser.setUsername("zxc");
      gotUser.setHash("p455");
      gotUser.setEnabled(true);
      gotUser.setName("vbnm");
      gotUser.setAdmin(true);

      MvcResult mvcResult = mockMvc.perform(
              put(USERS_URL).with(testAdmin()).with(csrf())
                      .contentType(APPLICATION_JSON_VALUE)
                      .content(jackson.writeValueAsString(gotUser)))
              .andExpect(status().isOk())
              .andExpect(content().contentType(APPLICATION_JSON_VALUE))
              .andReturn();
      DetailedUser modifiedUser = jackson.readValue(
              mvcResult.getResponse().getContentAsString(),
              DetailedUser.class);

      assertEquals("zxc", modifiedUser.getUsername());
      assertTrue(passwordEncoder.matches("p455", modifiedUser.getHash()));
      assertTrue(modifiedUser.getEnabled());
      assertEquals("vbnm", modifiedUser.getName());
      assertTrue(modifiedUser.getAdmin());
    } finally {
      deleteUser(userId);
    }
  }

  private Long createUser() throws Exception {
    MvcResult mvcResult = mockMvc.perform(
            post(USERS_URL).with(testAdmin()).with(csrf()))
            .andExpect(status().isOk())
            .andReturn();
    return jackson.readValue(
            mvcResult.getResponse().getContentAsString(),
            Long.class);
  }

  private DetailedUser retrieveUser(Long userId) throws Exception {
    MvcResult mvcResult = mockMvc.perform(
            get(USERS_URL + "/{id}", userId).with(testAdmin()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andReturn();
    return jackson.readValue(
            mvcResult.getResponse().getContentAsString(),
            DetailedUser.class);
  }

  private void deleteUser(Long userId) throws Exception {
    mockMvc.perform(
            delete(USERS_URL + "/{id}", userId).with(testAdmin()).with(csrf()))
            .andExpect(status().isOk());
    mockMvc.perform(
            get(USERS_URL + "/{id}", userId).with(testAdmin()))
            .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
            .andExpect(content().json("{\"i18nCode\":\"common.errorI18nCode.notFound\",\"args\":{\"id\":\"" + userId + "\",\"entity\":\"User\"}}"));
  }

}
