package com.odroid.tortuga.test.api;

import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.UserRequestPostProcessor;

import static com.odroid.tortuga.test.TestConstants.USERNAME_ADMIN;
import static com.odroid.tortuga.test.TestConstants.USERNAME_USER;
import static com.odroid.tortuga.test.api.ApiTestConstants.ROLE_ADMIN_SHORT;
import static com.odroid.tortuga.test.api.ApiTestConstants.ROLE_USER_SHORT;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;

public interface ApiTestUtils {

  static UserRequestPostProcessor testAdmin() {
    return user(USERNAME_ADMIN)
            .roles(ROLE_ADMIN_SHORT, ROLE_USER_SHORT);
  }

  static UserRequestPostProcessor testUser() {
    return user(USERNAME_USER)
            .roles(ROLE_USER_SHORT);
  }

}
