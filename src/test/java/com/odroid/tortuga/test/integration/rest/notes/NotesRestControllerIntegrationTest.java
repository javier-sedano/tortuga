package com.odroid.tortuga.test.integration.rest.notes;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.odroid.tortuga.SpringBootApp;
import com.odroid.tortuga.common.AppConfiguration;
import com.odroid.tortuga.domain.Note;
import com.odroid.tortuga.domain.User;
import com.odroid.tortuga.rest.notes.NotesRestController;
import com.odroid.tortuga.service.iface.errors.ConcurrencyException;
import com.odroid.tortuga.service.iface.errors.ForbiddenException;
import com.odroid.tortuga.service.iface.errors.NotFoundException;
import com.odroid.tortuga.service.iface.notes.DetailedNote;
import com.odroid.tortuga.service.iface.notes.MinimalNote;
import com.odroid.tortuga.service.iface.notes.WrongNoteException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.okJson;
import static com.github.tomakehurst.wiremock.client.WireMock.status;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static com.odroid.tortuga.test.AppTestPropertySource.TEST_PROPERTIES_SOURCE;
import static com.odroid.tortuga.test.TestConstants.ID_ADMIN;
import static com.odroid.tortuga.test.TestConstants.ID_USER;
import static com.odroid.tortuga.test.integration.IntegrationTestUtils.adminPrincipal;
import static com.odroid.tortuga.test.integration.IntegrationTestUtils.userPrincipal;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@SpringBootTest(classes = SpringBootApp.class)
@TestPropertySource(value = TEST_PROPERTIES_SOURCE, properties = {
        "app.lorem-base-url=http://localhost:" + NotesRestControllerIntegrationTest.LOREM_PORT + "/"
})
class NotesRestControllerIntegrationTest {
  static final int LOREM_PORT = 8082;
  private static final String LOREM_API_URL = "/api/";
  private static final String DEFAULT_TITLE = ".";
  private static final String DEFAULT_CONTENT = "";

  @PersistenceContext
  private EntityManager em;

  @Autowired
  private NotesRestController notesController;

  @Autowired
  private AppConfiguration appConfiguration;

  static WireMockServer loremMock;

  @BeforeAll
  static void startWireMock() {
    loremMock = new WireMockServer(LOREM_PORT);
    loremMock.start();
  }

  @AfterAll
  static void stopWireMock() {
    loremMock.stop();
  }

  @AfterEach
  void resetWireMock() {
    loremMock.resetAll();
  }

  @Test
  @Transactional
  void testFindMyNotes() {
    assertEquals(new ArrayList<>(), notesController.find("", 0, adminPrincipal()));
    User admin = em.find(User.class, ID_ADMIN);
    User user = em.find(User.class, ID_USER);
    Note note1 = Note.builder().title("qwer").content("tyuiop").user(admin).build();
    em.persist(note1);
    Note note2 = Note.builder().title("asdf").content("ghjkl").user(admin).build();
    em.persist(note2);
    Note note3 = Note.builder().title("ASDF").content("GHJKL").user(admin).build();
    em.persist(note3);
    Note note4 = Note.builder().title("asdf").content("ghjkl").user(user).build();
    em.persist(note4);
    assertEquals(
            Arrays.asList(MinimalNote.fromNote(note3), MinimalNote.fromNote(note2), MinimalNote.fromNote(note1)),
            notesController.find("", 0, adminPrincipal())
    );
    assertEquals(
            Arrays.asList(MinimalNote.fromNote(note3), MinimalNote.fromNote(note2)),
            notesController.find("asdf", 0, adminPrincipal())
    );
    assertEquals(
            Collections.singletonList(MinimalNote.fromNote(note4)),
            notesController.find("asdf", 0, userPrincipal())
    );
  }

  @Test
  @Transactional
  void testFindNotesFilter() {
    assertEquals(new ArrayList<>(), notesController.find("", 0, adminPrincipal()));
    User admin = em.find(User.class, ID_ADMIN);
    Note note1 = Note.builder().title("qwer").content("tyuiop").user(admin).build();
    em.persist(note1);
    Note note2 = Note.builder().title("asdf").content("ghjkl").user(admin).build();
    em.persist(note2);
    Note note3 = Note.builder().title("ASDF").content("GHJKL").user(admin).build();
    em.persist(note3);
    assertEquals(
            new ArrayList<>(),
            notesController.find("zxcvbnm", 0, adminPrincipal())
    );
    assertEquals(
            Collections.singletonList(MinimalNote.fromNote(note1)),
            notesController.find("q", 0, adminPrincipal())
    );
    assertEquals(
            Collections.singletonList(MinimalNote.fromNote(note1)),
            notesController.find("r", 0, adminPrincipal())
    );
    assertEquals(
            Collections.singletonList(MinimalNote.fromNote(note1)),
            notesController.find("we", 0, adminPrincipal())
    );
    assertEquals(
            Collections.singletonList(MinimalNote.fromNote(note1)),
            notesController.find("t", 0, adminPrincipal())
    );
    assertEquals(
            Collections.singletonList(MinimalNote.fromNote(note1)),
            notesController.find("p", 0, adminPrincipal())
    );
    assertEquals(
            Collections.singletonList(MinimalNote.fromNote(note1)),
            notesController.find("ui", 0, adminPrincipal())
    );
  }

  @Test
  @Transactional
  void testFindNotesCaseInsensitiveFilter() {
    assertEquals(new ArrayList<>(), notesController.find("", 0, adminPrincipal()));
    User admin = em.find(User.class, ID_ADMIN);
    Note note1 = Note.builder().title("qwer").content("tyuiop").user(admin).build();
    em.persist(note1);
    Note note2 = Note.builder().title("asdf").content("ghjkl").user(admin).build();
    em.persist(note2);
    Note note3 = Note.builder().title("ASDF").content("GHJKL").user(admin).build();
    em.persist(note3);
    assertEquals(
            Arrays.asList(MinimalNote.fromNote(note3), MinimalNote.fromNote(note2)),
            notesController.find("asdf", 0, adminPrincipal())
    );
    assertEquals(
            Arrays.asList(MinimalNote.fromNote(note3), MinimalNote.fromNote(note2)),
            notesController.find("ASDF", 0, adminPrincipal())
    );
    assertEquals(
            Arrays.asList(MinimalNote.fromNote(note3), MinimalNote.fromNote(note2)),
            notesController.find("ghjkl", 0, adminPrincipal())
    );
    assertEquals(
            Arrays.asList(MinimalNote.fromNote(note3), MinimalNote.fromNote(note2)),
            notesController.find("GHJKL", 0, adminPrincipal())
    );
  }

  @Test
  @Transactional
  void testFindMyNotesWithPagination() {
    assertEquals(
            new ArrayList<>(),
            notesController.find("", 0, adminPrincipal())
    );
    User admin = em.find(User.class, ID_ADMIN);
    List<MinimalNote> insertedMinimalNotes = new ArrayList<>();
    for (int i = 0; i < 10 * appConfiguration.getPageSize(); i++) {
      Note note = Note.builder().title(String.format("title%03d", i)).content(String.format("content%03d", i)).user(admin).build();
      em.persist(note);
      insertedMinimalNotes.add(MinimalNote.fromNote(note));
    }
    assertEquals(
            insertedMinimalNotes.subList(0, appConfiguration.getPageSize()),
            notesController.find("title", 0, adminPrincipal())
    );
    assertEquals(
            insertedMinimalNotes.subList(appConfiguration.getPageSize(), appConfiguration.getPageSize() + appConfiguration.getPageSize()),
            notesController.find("title", appConfiguration.getPageSize(), adminPrincipal())
    );
    assertEquals(
            insertedMinimalNotes.subList(insertedMinimalNotes.size() - 3, insertedMinimalNotes.size()),
            notesController.find("title", insertedMinimalNotes.size() - 3, adminPrincipal())
    );
  }

  @Test
  @Transactional
  void testGetNote() {
    User admin = em.find(User.class, ID_ADMIN);
    Note note1 = Note.builder().title("qwer").content("tyuiop").user(admin).build();
    em.persist(note1);
    DetailedNote gotDetailedNote1 = notesController.retrieve(note1.getId(), adminPrincipal());
    assertEquals(DetailedNote.fromNote(note1), gotDetailedNote1);
  }

  @Test
  @Transactional
  void testGetHorrorNote() {
    User admin = em.find(User.class, ID_ADMIN);
    Note note1 = Note.builder().title("H0RR0R").content("tyuiop").user(admin).build();
    em.persist(note1);
    Long noteId = note1.getId();
    Principal adminPrincipal = adminPrincipal();
    assertThrows(WrongNoteException.class, () -> notesController.retrieve(noteId, adminPrincipal));
  }

  @Test
  @Transactional
  void testGetNoteNotExisting() {
    User admin = em.find(User.class, ID_ADMIN);
    Note note1 = Note.builder().title("qwer").content("tyuiop").user(admin).build();
    em.persist(note1);
    Long nonExistingNoteId = note1.getId() + 1;
    Principal adminPrincipal = adminPrincipal();
    assertThrows(NotFoundException.class, () -> notesController.retrieve(nonExistingNoteId, adminPrincipal));
  }

  @Test
  @Transactional
  void testGetNoteHacker() {
    User admin = em.find(User.class, ID_ADMIN);
    Note note1 = Note.builder().title("qwer").content("tyuiop").user(admin).build();
    em.persist(note1);
    Long noteId = note1.getId();
    Principal userPrincipal = userPrincipal();
    assertThrows(ForbiddenException.class, () -> notesController.retrieve(noteId, userPrincipal));
  }

  @Test
  @Transactional
  void testCreateWithLorem() {
    String wordsText = "Qwerty uiop.";
    String paragraphText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis itur.";
    loremMock.stubFor(get(urlPathEqualTo(LOREM_API_URL))
            .withQueryParam("type", equalTo("word"))
            .withQueryParam("length", equalTo("2"))
            .withQueryParam("startLorem", equalTo("false"))
            .withHeader("Accept", equalTo(APPLICATION_JSON_VALUE))
            .willReturn(okJson(
                    "{\"version\": 2, \"text\": \"" + wordsText + "\", \"requestParams\": {\"type\": \"word\", \"length\": \"2\" } }"
            ))
    );
    loremMock.stubFor(get(urlPathEqualTo(LOREM_API_URL))
            .withQueryParam("type", equalTo("paragraph"))
            .withQueryParam("length", equalTo("1"))
            .withQueryParam("startLorem", equalTo("true"))
            .withHeader("Accept", equalTo(APPLICATION_JSON_VALUE))
            .willReturn(okJson(
                    "{\"version\":2,\"text\":\"" + paragraphText + "\",\"requestParams\":{\"type\":\"paragraph\",\"length\":\"1\",\"startLorem\":\"true\"}}"
            ))
    );
    Long noteId = notesController.create(adminPrincipal());
    loremMock.verify(2, getRequestedFor(urlPathEqualTo(LOREM_API_URL)));
    Note gotNote = em.find(Note.class, noteId);
    assertEquals(wordsText, gotNote.getTitle());
    assertEquals(paragraphText, gotNote.getContent());
  }

  @Test
  @Transactional
  void testCreateWithLoremFailingWords() {
    loremMock.stubFor(get(urlPathEqualTo(LOREM_API_URL))
            .withQueryParam("type", equalTo("word"))
            .withQueryParam("length", equalTo("2"))
            .withQueryParam("startLorem", equalTo("false"))
            .withHeader("Accept", equalTo(APPLICATION_JSON_VALUE))
            .willReturn(status(HttpStatus.INTERNAL_SERVER_ERROR.value()))
    );
    Long noteId = notesController.create(adminPrincipal());
    Note gotNote = em.find(Note.class, noteId);
    assertEquals(DEFAULT_TITLE, gotNote.getTitle());
    assertEquals(DEFAULT_CONTENT, gotNote.getContent());
  }

  @Test
  @Transactional
  void testCreateWithLoremFailingParagraph() {
    String wordsText = "Qwerty uiop.";
    loremMock.stubFor(get(urlPathEqualTo(LOREM_API_URL))
            .withQueryParam("type", equalTo("word"))
            .withQueryParam("length", equalTo("2"))
            .withQueryParam("startLorem", equalTo("false"))
            .withHeader("Accept", equalTo(APPLICATION_JSON_VALUE))
            .willReturn(okJson(
                    "{\"version\": 2, \"text\": \"" + wordsText + "\", \"requestParams\": {\"type\": \"word\", \"length\": \"2\" } }"
            ))
    );
    loremMock.stubFor(get(urlPathEqualTo(LOREM_API_URL))
            .withQueryParam("type", equalTo("paragraph"))
            .withQueryParam("length", equalTo("1"))
            .withQueryParam("startLorem", equalTo("true"))
            .withHeader("Accept", equalTo(APPLICATION_JSON_VALUE))
            .willReturn(status(HttpStatus.INTERNAL_SERVER_ERROR.value()))
    );
    Long noteId = notesController.create(adminPrincipal());
    Note gotNote = em.find(Note.class, noteId);
    assertEquals(wordsText, gotNote.getTitle());
    assertEquals(DEFAULT_CONTENT, gotNote.getContent());
  }

  @Test
  @Transactional
  void testCreateWithoutLorem() {
    Long noteId = notesController.create(adminPrincipal());
    Note gotNote = em.find(Note.class, noteId);
    assertEquals(DEFAULT_TITLE, gotNote.getTitle());
    assertEquals(DEFAULT_CONTENT, gotNote.getContent());
  }

  @Test
  @Transactional
  void testUpdate() {
    User admin = em.find(User.class, ID_ADMIN);
    Note note1 = Note.builder().title("qwer").content("tyuiop").user(admin).build();
    em.persist(note1);
    DetailedNote detailedNote = DetailedNote.builder().id(note1.getId()).title("Lorem ipsum").content("dolor sit amet").version(note1.getVersion()).build();
    DetailedNote updatedNote = notesController.update(detailedNote, adminPrincipal());
    assertEquals("Lorem ipsum", updatedNote.getTitle());
    assertEquals("dolor sit amet", updatedNote.getContent());
    Note dbNote = em.find(Note.class, note1.getId());
    assertEquals("Lorem ipsum", dbNote.getTitle());
    assertEquals("dolor sit amet", dbNote.getContent());
  }


  @Test
  @Transactional
  void testUpdateWrongNote() {
    User admin = em.find(User.class, ID_ADMIN);
    Note note1 = Note.builder().title("3RR0R").content("wrong").user(admin).build();
    em.persist(note1);
    DetailedNote detailedNote = DetailedNote.builder().id(note1.getId()).title("3RR0R II").content("wrong II").version(1L).build();
    Principal adminPrincipal = adminPrincipal();
    assertThrows(WrongNoteException.class, () -> notesController.update(detailedNote, adminPrincipal));
  }

  @Test
  @Transactional
  void testUpdateHacker() {
    User admin = em.find(User.class, ID_ADMIN);
    Note note1 = Note.builder().title("qwer").content("tyuiop").user(admin).build();
    em.persist(note1);
    DetailedNote detailedNote = DetailedNote.builder().id(note1.getId()).title("Lorem ipsum").content("dolor sit amet").version(1L).build();
    Principal userPrincipal = userPrincipal();
    assertThrows(ForbiddenException.class, () -> notesController.update(detailedNote, userPrincipal));
  }

  @Test
  void testConcurrentUpdateDetected() {
    Long noteId = notesController.create(adminPrincipal());
    try {
      DetailedNote note1 = notesController.retrieve(noteId, adminPrincipal());
      DetailedNote note2 = notesController.retrieve(noteId, adminPrincipal());
      note1.setTitle("11");
      note2.setTitle("22");
      notesController.update(note1, adminPrincipal());
      DetailedNote note1Modified = notesController.retrieve(noteId, adminPrincipal());
      assertEquals(note1.getTitle(), note1Modified.getTitle());
      Principal adminPrincipal = adminPrincipal();
      assertThrows(ConcurrencyException.class, () -> notesController.update(note2, adminPrincipal));
    } finally {
      notesController.delete(noteId, adminPrincipal());
    }
  }

  @Test
  @Transactional
  void testDelete() {
    User admin = em.find(User.class, ID_ADMIN);
    Note note1 = Note.builder().title("qwer").content("tyuiop").user(admin).build();
    em.persist(note1);
    notesController.delete(note1.getId(), adminPrincipal());
    assertNull(em.find(Note.class, note1.getId()));
  }

  @Test
  @Transactional
  void testDeleteHacker() {
    User admin = em.find(User.class, ID_ADMIN);
    Note note1 = Note.builder().title("qwer").content("tyuiop").user(admin).build();
    em.persist(note1);
    Long noteId = note1.getId();
    Principal userPrincipal = userPrincipal();
    assertThrows(ForbiddenException.class, () -> notesController.delete(noteId, userPrincipal));
  }

}
