package com.odroid.tortuga.test.integration;

import com.odroid.tortuga.test.AppTestPropertySource;
import com.odroid.tortuga.SpringBootApp;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(classes = SpringBootApp.class)
@AppTestPropertySource
class SpringBootAppIntegrationTest {

  private static final Logger LOGGER = LoggerFactory.getLogger(SpringBootAppIntegrationTest.class);

  @Autowired
  ApplicationContext applicationContext;

  @Test
  void contextLoads() {
    assertNotNull(applicationContext);
  }
}
