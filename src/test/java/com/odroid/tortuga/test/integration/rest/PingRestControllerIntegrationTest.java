package com.odroid.tortuga.test.integration.rest;

import com.odroid.tortuga.test.AppTestPropertySource;
import com.odroid.tortuga.SpringBootApp;
import com.odroid.tortuga.rest.ping.PingRestController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest(classes = SpringBootApp.class)
@AppTestPropertySource
class PingRestControllerIntegrationTest {

  @Autowired
  private PingRestController pingRestController;

  @Test
  @Transactional
  void testVersion() {
    assertFalse(pingRestController.ver().isEmpty());
  }

}
