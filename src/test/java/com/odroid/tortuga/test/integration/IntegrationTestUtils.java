package com.odroid.tortuga.test.integration;

import com.odroid.tortuga.service.iface.users.MinimalUser;
import com.sun.security.auth.UserPrincipal; //NOSONAR

import java.security.Principal;

import static com.odroid.tortuga.test.TestConstants.ID_ADMIN;
import static com.odroid.tortuga.test.TestConstants.ID_DISABLED;
import static com.odroid.tortuga.test.TestConstants.ID_USER;
import static com.odroid.tortuga.test.TestConstants.USERNAME_ADMIN;
import static com.odroid.tortuga.test.TestConstants.USERNAME_DISABLED;
import static com.odroid.tortuga.test.TestConstants.USERNAME_USER;

public abstract class IntegrationTestUtils {

  public static final MinimalUser MINIMAL_USER_A = MinimalUser.builder().id(ID_ADMIN).username(USERNAME_ADMIN).build();
  public static final MinimalUser MINIMAL_USER_B = MinimalUser.builder().id(ID_USER).username(USERNAME_USER).build();
  public static final MinimalUser MINIMAL_USER_C = MinimalUser.builder().id(ID_DISABLED).username(USERNAME_DISABLED).build();

  private IntegrationTestUtils() {
    // Intentionally blank to avoid instantiation
  }

  public static Principal adminPrincipal() {
    return new UserPrincipal(USERNAME_ADMIN);
  }

  public static Principal userPrincipal() {
    return new UserPrincipal(USERNAME_USER);
  }

}
