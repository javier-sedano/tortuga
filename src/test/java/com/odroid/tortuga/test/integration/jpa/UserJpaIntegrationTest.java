package com.odroid.tortuga.test.integration.jpa;

import com.odroid.tortuga.test.AppTestPropertySource;
import com.odroid.tortuga.SpringBootApp;
import com.odroid.tortuga.domain.Authority;
import com.odroid.tortuga.domain.User;
import com.odroid.tortuga.domain.UserAuthority;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(classes = SpringBootApp.class)
@AppTestPropertySource
class UserJpaIntegrationTest {
  private static final Logger LOGGER = LoggerFactory.getLogger(UserJpaIntegrationTest.class);

  private static final Long AUSER_ID = 1L;
  private static final String AUSER_USERNAME = "a";

  @PersistenceContext
  private EntityManager em;

  @Test
  @Transactional
  void testMinimalUser() {
    User aUser = em.find(User.class, AUSER_ID);
    assertEquals(AUSER_USERNAME, aUser.getUsername());
    List<UserAuthority> authorities = aUser.getUserAuthorities();
    LOGGER.debug("A user: {}", aUser);
    assertTrue(authorities.stream().map(UserAuthority::getAuthority).anyMatch(Authority.ROLE_USER::equals));
    assertTrue(authorities.stream().map(UserAuthority::getAuthority).anyMatch(Authority.ROLE_ADMIN::equals));
  }

  @Test
  @Transactional
  void testSimpleJpaOperations() {
    User zUser = User.builder().username("z").hash("qwerty").enabled(true).name("Zacarias").build();
    em.persist(zUser);
    User gotZUser = em.find(User.class, zUser.getId());
    assertEquals(zUser.getUsername(), gotZUser.getUsername());
    assertEquals(zUser.getHash(), gotZUser.getHash());
    assertEquals(zUser.getEnabled(), gotZUser.getEnabled());
    assertEquals(zUser.getName(), gotZUser.getName());
    assertEquals(zUser, gotZUser);
  }

}
