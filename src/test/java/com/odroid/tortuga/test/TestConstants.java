package com.odroid.tortuga.test;

public class TestConstants {
  public static final String USERNAME_ADMIN = "a";
  public static final Long ID_ADMIN = 1L;
  public static final String USERNAME_USER = "b";
  public static final Long ID_USER = 2L;
  public static final String USERNAME_DISABLED = "c";
  public static final Long ID_DISABLED = 3L;

  private TestConstants() {
    // Intentionally blank to avoid instantiation
  }

}
