package com.odroid.tortuga.test;

import org.springframework.test.context.TestPropertySource;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static com.odroid.tortuga.test.AppTestPropertySource.TEST_PROPERTIES_SOURCE;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@TestPropertySource(TEST_PROPERTIES_SOURCE)
public @interface AppTestPropertySource {
  String TEST_PROPERTIES_SOURCE = "classpath:test.properties";
}
