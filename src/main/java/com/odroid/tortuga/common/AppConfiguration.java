package com.odroid.tortuga.common;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

@Component
@ConfigurationProperties(prefix = "app")
@Data
@Validated
public class AppConfiguration {

  private static final Integer DEFAUT_PAGE_SIZE = 20;

  @NotNull
  private Integer pageSize = DEFAUT_PAGE_SIZE;

  @NotNull
  private String loremBaseUrl;
}
