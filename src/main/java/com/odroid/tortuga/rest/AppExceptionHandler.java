package com.odroid.tortuga.rest;

import com.odroid.tortuga.service.iface.errors.AppException;
import com.odroid.tortuga.service.iface.errors.ErrorDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.annotation.ResponseStatusExceptionResolver;

@ControllerAdvice
public class AppExceptionHandler extends ResponseStatusExceptionResolver {
  private static final Logger LOGGER = LoggerFactory.getLogger(AppExceptionHandler.class);

  @ExceptionHandler(AppException.class)
  @ResponseBody
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ErrorDetails appExceptionHandler(AppException e) {
    LOGGER.error(e.getMessage(), e);
    return e.getErrorDetails();
  }
}
