package com.odroid.tortuga.rest.users;

import com.odroid.tortuga.common.AppConfiguration;
import com.odroid.tortuga.rest.LinkedDataException;
import com.odroid.tortuga.service.iface.users.DetailedUser;
import com.odroid.tortuga.service.iface.users.MinimalUser;
import com.odroid.tortuga.service.iface.users.UsersService;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.odroid.tortuga.rest.RestConstants.BASE_REST_URL;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(BASE_REST_URL + "users")
public class UsersRestController {

  private final UsersService usersService;
  private final AppConfiguration appConfiguration;

  public UsersRestController(UsersService usersService, AppConfiguration appConfiguration) {
    this.usersService = usersService;
    this.appConfiguration = appConfiguration;
  }

  @GetMapping(value = "", produces = {APPLICATION_JSON_VALUE})
  public List<MinimalUser> find(
          @RequestParam(value = "filter", defaultValue = "") String filter,
          @RequestParam(value = "from", defaultValue = "0") long from
  ) {
    return usersService.findUsers(filter.replaceAll("[\n\r\t]", "_"), from, appConfiguration.getPageSize());
  }

  @GetMapping(value = "/{id}", produces = {APPLICATION_JSON_VALUE})
  public DetailedUser retrieve(@PathVariable("id") Long id) {
    return usersService.getUser(id);
  }

  @PostMapping(value = "", produces = {APPLICATION_JSON_VALUE})
  public Long create() {
    return usersService.createUser();
  }

  @PutMapping(value = "", consumes = {APPLICATION_JSON_VALUE})
  public DetailedUser update(@RequestBody DetailedUser user) {
    try {
      usersService.updateUser(user);
      return usersService.getUser(user.getId());
    } catch (DataIntegrityViolationException e) {
      if (e.getCause() instanceof ConstraintViolationException) {
        ConstraintViolationException cve = (ConstraintViolationException) e.getCause();
        if (cve.getConstraintName().equalsIgnoreCase("users_username_unique")) {
          throw new UsernameCollisionException();
        }
      }
      throw e;
    }
  }

  @DeleteMapping(value = "/{id}")
  public void delete(@PathVariable("id") Long id) {
    try {
      usersService.deleteUser(id);
    } catch (DataIntegrityViolationException e) {
      throw new LinkedDataException(e);
    }
  }

}
