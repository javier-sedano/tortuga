package com.odroid.tortuga.rest.users;

import com.odroid.tortuga.service.iface.errors.AppException;

public class UsernameCollisionException extends AppException {

  private static final String I18N = "admin.users.errorI18nCode.usernameCollision";

  public UsernameCollisionException() {
    super(I18N);
  }
}
