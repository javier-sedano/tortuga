package com.odroid.tortuga.repository;

import com.odroid.tortuga.domain.UserAuthority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersAuthoritiesRepository extends JpaRepository<UserAuthority, Long> {
}
