package com.odroid.tortuga.domain;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import java.io.Serializable;

@Entity
@Table(name = "notes")
@Data
@NoArgsConstructor
public class Note implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column
  @NonNull
  private String title;

  @Column
  @NonNull
  private String content;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id", referencedColumnName = "id")
  @NonNull
  private User user;

  @Version
  @NonNull
  @Setter(AccessLevel.NONE)
  private Long version;

  @Builder
  private Note(String title, String content, User user, Long version) {
    this.title = title;
    this.content = content;
    this.user = user;
    this.version = version;
  }
}
