package com.odroid.tortuga.service.impl.notes;


import com.odroid.tortuga.domain.Note;
import com.odroid.tortuga.repository.NotesRepository;
import com.odroid.tortuga.repository.UsersRepository;
import com.odroid.tortuga.service.iface.errors.ConcurrencyException;
import com.odroid.tortuga.service.iface.errors.ForbiddenException;
import com.odroid.tortuga.service.iface.errors.NotFoundException;
import com.odroid.tortuga.service.iface.lorem.LoremService;
import com.odroid.tortuga.service.iface.notes.DetailedNote;
import com.odroid.tortuga.service.iface.notes.MinimalNote;
import com.odroid.tortuga.service.iface.notes.NotesService;
import com.odroid.tortuga.service.iface.notes.WrongNoteException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Transactional(readOnly = true)
public class NotesServiceJpaImpl implements NotesService {

  private static final Logger LOGGER = LoggerFactory.getLogger(NotesServiceJpaImpl.class);
  private static final String NOTE = "Note";
  private static final String WRONG_NOTE_IDENTIFIER = "3RR0R";
  private static final String HORROR_NOTE_IDENTIFIER = "H0RR0R";

  private final NotesRepository notesRepository;

  private final UsersRepository usersRepository;

  private final LoremService loremService;

  public NotesServiceJpaImpl(NotesRepository notesRepository, UsersRepository usersRepository,
                             LoremService loremService) {
    this.notesRepository = notesRepository;
    this.usersRepository = usersRepository;
    this.loremService = loremService;
  }

  @Override
  public List<MinimalNote> findNotes(String filter, String username, long from, long limit) {
    LOGGER.debug("Filtering {} notes from {} for {} with {}", limit, from, username, filter);
    try (Stream<Note> notesStream = notesRepository.findNotes(filter, username)) {
      List<MinimalNote> notes = notesStream
              .skip(from)
              .limit(limit)
              .map(MinimalNote::fromNote)
              .collect(Collectors.toList());
      LOGGER.debug("Filtered notes: {}", notes);
      return notes;
    }
  }

  @Override
  public DetailedNote getNote(Long id, String username) {
    LOGGER.debug("Getting note {} for {}", id, username);
    var note = DetailedNote.fromNote(getNoteOwnedBy(id, username));
    if (note.getTitle().contains(HORROR_NOTE_IDENTIFIER)) {
      throw new WrongNoteException(note.getTitle());
    }
    LOGGER.debug("Got note: {}", note);
    return note;
  }

  private Note getNoteOwnedBy(Long id, String username, Long version) {
    var note = getNoteOwnedBy(id, username);
    if (!version.equals(note.getVersion())) {
      throw new ConcurrencyException();
    }
    return note;
  }

  private Note getNoteOwnedBy(Long id, String username) {
    var note = notesRepository.findById(id).orElseThrow(() -> new NotFoundException(NOTE, id));
    if (!note.getUser().getUsername().equals(username)) {
      throw new ForbiddenException();
    }
    return note;
  }

  @Override
  @Transactional
  public Long createNote(String username) {
    LOGGER.debug("Creating note for {}", username);
    var user = usersRepository.findByUsername(username);
    var title = ".";
    var content = "";
    try {
      title = loremService.generateWords(2);
      content = loremService.generateParagraph(true);
    } catch (IOException e) {
      LOGGER.error(e.getMessage(), e);
      // Just proceed with default values
    }
    var note = Note.builder().title(title).content(content).user(user).build();
    var savedNote = notesRepository.save(note);
    Long noteId = savedNote.getId();
    LOGGER.debug("Created note {}", noteId);
    return noteId;
  }

  @Override
  @Transactional
  public void updateNote(DetailedNote detailedNote, String username) {
    LOGGER.debug("Updating note {} for {}", detailedNote, username);
    if (detailedNote.getTitle().contains(WRONG_NOTE_IDENTIFIER)) {
      throw new WrongNoteException(detailedNote.getTitle());
    }
    var note = getNoteOwnedBy(detailedNote.getId(), username, detailedNote.getVersion());
    note.setTitle(detailedNote.getTitle());
    note.setContent(detailedNote.getContent());
    LOGGER.debug("Updated note {}", detailedNote);
  }

  @Transactional
  public void deleteNote(Long id, String username) {
    LOGGER.debug("Deleting note {} for {}", id, username);
    var note = getNoteOwnedBy(id, username);
    notesRepository.delete(note);
    LOGGER.debug("Deleted note {}", id);
  }


}
