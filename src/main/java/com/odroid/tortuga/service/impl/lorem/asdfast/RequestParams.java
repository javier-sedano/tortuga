package com.odroid.tortuga.service.impl.lorem.asdfast;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RequestParams {

  private String type;

  private String length;

  private String startLorem;
}
