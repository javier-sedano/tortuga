package com.odroid.tortuga.service.impl.lorem.asdfast;

public final class TypeConstants {
  private TypeConstants() {
  }

  public static final String WORD = "word";
  public static final String PARAGRAPH = "paragraph";
}
