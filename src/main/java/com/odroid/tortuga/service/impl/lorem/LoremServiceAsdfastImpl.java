package com.odroid.tortuga.service.impl.lorem;

import com.odroid.tortuga.service.iface.lorem.LoremService;
import com.odroid.tortuga.service.impl.lorem.asdfast.AsdfastLoremGenerator;
import com.odroid.tortuga.service.impl.lorem.asdfast.Lorem;
import org.springframework.stereotype.Service;
import retrofit2.Response;

import java.io.IOException;

import static com.odroid.tortuga.service.impl.lorem.asdfast.TypeConstants.PARAGRAPH;
import static com.odroid.tortuga.service.impl.lorem.asdfast.TypeConstants.WORD;

@Service
public class LoremServiceAsdfastImpl implements LoremService {

  private final AsdfastLoremGenerator asdfastLoremGenerator;

  public LoremServiceAsdfastImpl(AsdfastLoremGenerator asdfastLoremGenerator) {
    this.asdfastLoremGenerator = asdfastLoremGenerator;
  }

  @Override
  public String generateWords(Integer count) throws IOException {
    Response<Lorem> response = asdfastLoremGenerator.generate(WORD, count, false).execute();
    if (!response.isSuccessful()) {
      throw new IOException(response.message());
    }
    return response.body().getText();
  }

  @Override
  public String generateParagraph(Boolean withLorem) throws IOException {
    Response<Lorem> response = asdfastLoremGenerator.generate(PARAGRAPH, 1, true).execute();
    if (!response.isSuccessful()) {
      throw new IOException(response.message());
    }
    return response.body().getText();
  }
}
