package com.odroid.tortuga.service.iface.notes;

import com.odroid.tortuga.service.iface.errors.AppException;

import java.util.AbstractMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WrongNoteException extends AppException {

  private static final String I18N = "notes.errorI18nCode.wrong";

  private static final String TITLE = "title";

  public WrongNoteException(String title) {
    super(
            I18N,
            Stream.of(
                    new AbstractMap.SimpleEntry<>(TITLE, title)
            ).collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue))
    );
  }
}
