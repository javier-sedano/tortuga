package com.odroid.tortuga.service.iface.errors;

import java.util.AbstractMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NotFoundException extends AppException {

  private static final String I18N = "common.errorI18nCode.notFound";

  public NotFoundException(String entity, Long id) {
    super(
            I18N,
            Stream.of(
                    new AbstractMap.SimpleEntry<>("entity", entity),
                    new AbstractMap.SimpleEntry<>("id", id.toString())
            ).collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue))
    );
  }
}
