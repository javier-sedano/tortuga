package com.odroid.tortuga.service.iface.errors;

public class ForbiddenException extends AppException {

  private static final String I18N = "common.errorI18nCode.forbidden";

  public ForbiddenException() {
    super(I18N);
  }
}
