package com.odroid.tortuga.service.iface.users;

import com.odroid.tortuga.domain.User;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
public class MinimalUser {

  @NonNull
  private Long id;

  @NonNull
  private String username;

  public static MinimalUser fromUser(User user) {
    return builder().id(user.getId()).username(user.getUsername()).build();
  }

}
