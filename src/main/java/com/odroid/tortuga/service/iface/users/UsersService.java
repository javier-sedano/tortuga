package com.odroid.tortuga.service.iface.users;

import java.util.List;

public interface UsersService {

  List<MinimalUser> findUsers(String filter, long from, long limit);

  DetailedUser getUser(Long id);

  Long createUser();

  void updateUser(DetailedUser user);

  void deleteUser(Long id);

}
