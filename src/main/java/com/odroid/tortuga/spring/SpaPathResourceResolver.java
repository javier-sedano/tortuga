package com.odroid.tortuga.spring;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.web.servlet.resource.PathResourceResolver;

import java.io.IOException;

@Slf4j
public class SpaPathResourceResolver extends PathResourceResolver {
  private final String mainClassPath;

  public SpaPathResourceResolver(String mainClassPath) {
    this.mainClassPath = mainClassPath;
  }

  @Override
  protected Resource getResource(String resourcePath, Resource location) throws IOException {
    var requestedResource = location.createRelative(resourcePath);
    if (requestedResource.exists()) {
      log.debug("Looking for {} at {} resolved as {}: found", resourcePath, location, requestedResource);
      return requestedResource;
    }
    log.debug("Looking for {} at {} resolved as {}: not found, using {}", resourcePath, location, requestedResource, mainClassPath);
    return new ClassPathResource(mainClassPath);
  }

}
