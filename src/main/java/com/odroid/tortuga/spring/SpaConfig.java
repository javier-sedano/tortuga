package com.odroid.tortuga.spring;

import com.odroid.tortuga.common.AppConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import static com.odroid.tortuga.rest.RestConstants.BASE_URL;
import static com.odroid.tortuga.rest.RestConstants.BASE_WEB_URL;

@Configuration
@Slf4j
public class SpaConfig implements WebMvcConfigurer {

  private static final String ROOT_URL = "/";
  private static final String MAIN_HTML = "index.html";
  private static final String REDIRECT = "redirect:";

  @Autowired
  private AppConfiguration appConfiguration;

  @Override
  public void addViewControllers(ViewControllerRegistry registry) {
    registry.addViewController(ROOT_URL).setViewName(REDIRECT + BASE_URL);
    registry.addViewController(BASE_URL).setViewName(REDIRECT + BASE_WEB_URL);
  }

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler(BASE_WEB_URL)
            .addResourceLocations("classpath:/static" + BASE_WEB_URL)
            .resourceChain(true)
            .addResolver(new SpaPathResourceResolver("/static" + BASE_WEB_URL + MAIN_HTML));
    registry.addResourceHandler(BASE_WEB_URL + "**")
            .addResourceLocations("classpath:/static" + BASE_WEB_URL)
            .resourceChain(true)
            .addResolver(new SpaPathResourceResolver("/static" + BASE_WEB_URL + MAIN_HTML));
  }

}
