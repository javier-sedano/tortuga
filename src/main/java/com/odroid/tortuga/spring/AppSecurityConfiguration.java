package com.odroid.tortuga.spring;

import com.odroid.tortuga.domain.Authority;
import com.odroid.tortuga.security.LoggedUserProviderAuthenticationSuccessHandler;
import com.odroid.tortuga.security.Status200LogoutSuccessHandler;
import com.odroid.tortuga.security.Status401AuthenticationFailureHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

import static com.odroid.tortuga.rest.RestConstants.BASE_REST_URL;
import static com.odroid.tortuga.rest.RestConstants.BASE_URL;
import static com.odroid.tortuga.rest.RestConstants.BASE_WEB_URL;
import static com.odroid.tortuga.rest.RestConstants.ROOT_URL;

@Configuration
@Order(2)
public class AppSecurityConfiguration extends WebSecurityConfigurerAdapter {

  private static final String WEB_URL_PATTERN = BASE_WEB_URL + "**";
  private static final String PUBLIC_REST_URL_PATTERN = BASE_REST_URL + "ping/**";
  private static final String LOGIN_URL = BASE_REST_URL + "login";
  private static final String LOGOUT_URL = BASE_REST_URL + "logout";
  private static final String SESSION_COOKIE = "JSESSIONID";
  private static final String USERNAME_PARAMETER = "username";
  private static final String PASS_PARAMETER = "password";


  @Override
  protected void configure(HttpSecurity http) throws Exception {
    // @formatter:off
    http
      .exceptionHandling()
        .authenticationEntryPoint(restAuthenticationEntryPoint())
      .and().authorizeRequests()
        .antMatchers(ROOT_URL, BASE_URL, WEB_URL_PATTERN, PUBLIC_REST_URL_PATTERN).permitAll()
        .anyRequest().hasRole(Authority.ROLE_USER.getShortForm())
      .and().formLogin()
        .loginProcessingUrl(LOGIN_URL)
        .usernameParameter(USERNAME_PARAMETER)
        .passwordParameter(PASS_PARAMETER)
        .permitAll()
        .successHandler(restAuthenticationSuccessHandler())
        .failureHandler(restAuthenticationFailureHandler())
      .and().logout()
        .logoutUrl(LOGOUT_URL)
        .deleteCookies(SESSION_COOKIE)
        .permitAll()
        .logoutSuccessHandler(logoutSuccessHandler())
      .and().csrf()
        .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
    // @formatter:on
  }

  @Bean
  public AuthenticationEntryPoint restAuthenticationEntryPoint() {
    return new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED);
  }

  @Bean
  public AuthenticationSuccessHandler restAuthenticationSuccessHandler() {
    return new LoggedUserProviderAuthenticationSuccessHandler();
  }

  @Bean
  public AuthenticationFailureHandler restAuthenticationFailureHandler() {
    return new Status401AuthenticationFailureHandler();
  }

  @Bean
  public LogoutSuccessHandler logoutSuccessHandler() {
    return new Status200LogoutSuccessHandler();
  }

}
