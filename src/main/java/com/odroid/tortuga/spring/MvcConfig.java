package com.odroid.tortuga.spring;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.filter.ForwardedHeaderFilter;

@Configuration
public class MvcConfig {
  @Bean
  public FilterRegistrationBean<ForwardedHeaderFilter> forwardedHeaderFilterFilterRegistrationBean() {
    var forwardedHeaderFilter = new ForwardedHeaderFilter();
    FilterRegistrationBean<ForwardedHeaderFilter> bean = new FilterRegistrationBean<>(forwardedHeaderFilter);
    bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
    return bean;
  }
}
