INSERT INTO users(id, username, hash, enabled, name, version) VALUES (4, 'd', '$2y$10$CGJXdWNwIIni2Ps7Mvhb7O.Zh2Fi9iiHg5v8ud643CWuKqPpH0KYm', false, 'D', 1);
INSERT INTO user_authorities(user_id, authority) VALUES (4, 'ROLE_USER');
INSERT INTO users(id, username, hash, enabled, name, version) VALUES (5, 'e', '$2y$10$CGJXdWNwIIni2Ps7Mvhb7O.Zh2Fi9iiHg5v8ud643CWuKqPpH0KYm', false, 'E', 1);
INSERT INTO user_authorities(user_id, authority) VALUES (5, 'ROLE_USER');
INSERT INTO users(id, username, hash, enabled, name, version) VALUES (6, 'f', '$2y$10$CGJXdWNwIIni2Ps7Mvhb7O.Zh2Fi9iiHg5v8ud643CWuKqPpH0KYm', false, 'F', 1);
INSERT INTO user_authorities(user_id, authority) VALUES (6, 'ROLE_USER');
INSERT INTO users(id, username, hash, enabled, name, version) VALUES (7, 'g', '$2y$10$CGJXdWNwIIni2Ps7Mvhb7O.Zh2Fi9iiHg5v8ud643CWuKqPpH0KYm', false, 'G', 1);
INSERT INTO user_authorities(user_id, authority) VALUES (7, 'ROLE_USER');
INSERT INTO users(id, username, hash, enabled, name, version) VALUES (8, 'h', '$2y$10$CGJXdWNwIIni2Ps7Mvhb7O.Zh2Fi9iiHg5v8ud643CWuKqPpH0KYm', false, 'H', 1);
INSERT INTO user_authorities(user_id, authority) VALUES (8, 'ROLE_USER');
INSERT INTO users(id, username, hash, enabled, name, version) VALUES (9, 'i', '$2y$10$CGJXdWNwIIni2Ps7Mvhb7O.Zh2Fi9iiHg5v8ud643CWuKqPpH0KYm', false, 'I', 1);
INSERT INTO user_authorities(user_id, authority) VALUES (9, 'ROLE_USER');
INSERT INTO users(id, username, hash, enabled, name, version) VALUES (10, 'j', '$2y$10$CGJXdWNwIIni2Ps7Mvhb7O.Zh2Fi9iiHg5v8ud643CWuKqPpH0KYm', false, 'J', 1);
INSERT INTO user_authorities(user_id, authority) VALUES (10, 'ROLE_USER');
INSERT INTO users(id, username, hash, enabled, name, version) VALUES (11, 'k', '$2y$10$CGJXdWNwIIni2Ps7Mvhb7O.Zh2Fi9iiHg5v8ud643CWuKqPpH0KYm', false, 'K', 1);
INSERT INTO user_authorities(user_id, authority) VALUES (11, 'ROLE_USER');
INSERT INTO users(id, username, hash, enabled, name, version) VALUES (12, 'l', '$2y$10$CGJXdWNwIIni2Ps7Mvhb7O.Zh2Fi9iiHg5v8ud643CWuKqPpH0KYm', false, 'L', 1);
INSERT INTO user_authorities(user_id, authority) VALUES (12, 'ROLE_USER');
INSERT INTO users(id, username, hash, enabled, name, version) VALUES (13, 'm', '$2y$10$CGJXdWNwIIni2Ps7Mvhb7O.Zh2Fi9iiHg5v8ud643CWuKqPpH0KYm', false, 'M', 1);
INSERT INTO user_authorities(user_id, authority) VALUES (13, 'ROLE_USER');
INSERT INTO users(id, username, hash, enabled, name, version) VALUES (14, 'n', '$2y$10$CGJXdWNwIIni2Ps7Mvhb7O.Zh2Fi9iiHg5v8ud643CWuKqPpH0KYm', false, 'N', 1);
INSERT INTO user_authorities(user_id, authority) VALUES (14, 'ROLE_USER');
INSERT INTO users(id, username, hash, enabled, name, version) VALUES (15, 'o', '$2y$10$CGJXdWNwIIni2Ps7Mvhb7O.Zh2Fi9iiHg5v8ud643CWuKqPpH0KYm', false, 'O', 1);
INSERT INTO user_authorities(user_id, authority) VALUES (15, 'ROLE_USER');
INSERT INTO users(id, username, hash, enabled, name, version) VALUES (16, 'p', '$2y$10$CGJXdWNwIIni2Ps7Mvhb7O.Zh2Fi9iiHg5v8ud643CWuKqPpH0KYm', false, 'P', 1);
INSERT INTO user_authorities(user_id, authority) VALUES (16, 'ROLE_USER');
INSERT INTO users(id, username, hash, enabled, name, version) VALUES (17, 'q', '$2y$10$CGJXdWNwIIni2Ps7Mvhb7O.Zh2Fi9iiHg5v8ud643CWuKqPpH0KYm', false, 'Q', 1);
INSERT INTO user_authorities(user_id, authority) VALUES (17, 'ROLE_USER');
INSERT INTO users(id, username, hash, enabled, name, version) VALUES (18, 'r', '$2y$10$CGJXdWNwIIni2Ps7Mvhb7O.Zh2Fi9iiHg5v8ud643CWuKqPpH0KYm', false, 'R', 1);
INSERT INTO user_authorities(user_id, authority) VALUES (18, 'ROLE_USER');
INSERT INTO users(id, username, hash, enabled, name, version) VALUES (19, 's', '$2y$10$CGJXdWNwIIni2Ps7Mvhb7O.Zh2Fi9iiHg5v8ud643CWuKqPpH0KYm', false, 'S', 1);
INSERT INTO user_authorities(user_id, authority) VALUES (19, 'ROLE_USER');
INSERT INTO users(id, username, hash, enabled, name, version) VALUES (20, 't', '$2y$10$CGJXdWNwIIni2Ps7Mvhb7O.Zh2Fi9iiHg5v8ud643CWuKqPpH0KYm', false, 'T', 1);
INSERT INTO user_authorities(user_id, authority) VALUES (20, 'ROLE_USER');
INSERT INTO users(id, username, hash, enabled, name, version) VALUES (21, 'u', '$2y$10$CGJXdWNwIIni2Ps7Mvhb7O.Zh2Fi9iiHg5v8ud643CWuKqPpH0KYm', false, 'U', 1);
INSERT INTO user_authorities(user_id, authority) VALUES (21, 'ROLE_USER');
INSERT INTO users(id, username, hash, enabled, name, version) VALUES (22, 'v', '$2y$10$CGJXdWNwIIni2Ps7Mvhb7O.Zh2Fi9iiHg5v8ud643CWuKqPpH0KYm', false, 'V', 1);
INSERT INTO user_authorities(user_id, authority) VALUES (22, 'ROLE_USER');
INSERT INTO users(id, username, hash, enabled, name, version) VALUES (23, 'w', '$2y$10$CGJXdWNwIIni2Ps7Mvhb7O.Zh2Fi9iiHg5v8ud643CWuKqPpH0KYm', false, 'W', 1);
INSERT INTO user_authorities(user_id, authority) VALUES (23, 'ROLE_USER');
INSERT INTO users(id, username, hash, enabled, name, version) VALUES (24, 'x', '$2y$10$CGJXdWNwIIni2Ps7Mvhb7O.Zh2Fi9iiHg5v8ud643CWuKqPpH0KYm', false, 'X', 1);
INSERT INTO user_authorities(user_id, authority) VALUES (24, 'ROLE_USER');
INSERT INTO users(id, username, hash, enabled, name, version) VALUES (25, 'y', '$2y$10$CGJXdWNwIIni2Ps7Mvhb7O.Zh2Fi9iiHg5v8ud643CWuKqPpH0KYm', false, 'Y', 1);
INSERT INTO user_authorities(user_id, authority) VALUES (25, 'ROLE_USER');
INSERT INTO users(id, username, hash, enabled, name, version) VALUES (26, 'z', '$2y$10$CGJXdWNwIIni2Ps7Mvhb7O.Zh2Fi9iiHg5v8ud643CWuKqPpH0KYm', false, 'Z', 1);
INSERT INTO user_authorities(user_id, authority) VALUES (26, 'ROLE_USER');
